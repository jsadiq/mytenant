<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->middleware('auth');

Route::get('invoice/{invoice_id}', 'OperationController@show_invoice')->name('invoice');

Route::prefix('management')->group(function(){
    Route::group(['middleware' => ['auth','role:manager']], function () {
        Route::get('report', 'ManagementController@showReport')->name('report');
        Route::get('generate-report', 'ManagementController@generateReport')->name('generateReport');
        Route::get('settings','ManagementController@settings')->name('settings');
        //Route::post('settings','ManagementController@settings');
        //Route::post('send-sms','ManagementController@send_sms');
    });

});
Route::group(['middleware' => ['auth','permission:manage shops']], function () {
    Route::get('shops','ShopController@shopList')->name('shop-list');
    Route::post('shop/save','ShopController@save')->name('shop-save');
    Route::post('shop/update-amount','ShopController@update_amount')->name('shop-update-amount');
});
Route::group(['middleware' => ['auth','permission:manage tenants']], function () {
    Route::get('clients','TenantController@tenantList')->name('tenant-list');
    Route::post('client/save','TenantController@save')->name('tenant-save');
});
Route::group(['middleware' => ['auth','permission:create deed']], function () {
    Route::get('booking','LedgerController@createBooking')->name('booking');
    Route::post('booking','LedgerController@saveBooking')->name('save-booking');
    Route::get('deed','LedgerController@createDeed')->name('deed');
    Route::post('deed','LedgerController@saveDeed')->name('save-deed');
    Route::any('create-ledger-entries','LedgerController@createEntries')->name('create-ledger-entries');
    Route::any('delete-ledger','LedgerController@deleteLedger')->name('delete-ledger');
    Route::any('create-other-ledger','LedgerController@createOtherLedger')->name('create-other-ledger');
});

Route::get('ledgers/{ledger_type}','LedgerController@ledgerList')->name('ledgers');
Route::get('ledger/{ledger_id}','LedgerController@showLedger')->name('ledger');
Route::any('payment','LedgerController@collectPayment')->name('payment');

Route::any('revenue-report','LedgerController@revenueList')->name('revenue-report');
Route::any('overdue-report','LedgerController@overdueList')->name('overdue-report');

Route::post('/api/{action}','apiController@index')->middleware('auth');

//Auth::routes();

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::post('/register','HomeController@register')->middleware(['auth','role:super_admin'])->name('register');
Route::post('/edit-user','HomeController@editUser')->middleware(['auth','role:super_admin'])->name('edit-user');
Route::get('/change-password','HomeController@updatePassword')->middleware(['auth']);
Route::post('/change-password','HomeController@updatePassword')->middleware(['auth'])->name('change-password');

Route::post('/send-sms','ManagementController@send_sms')->middleware(['auth', 'permission:send sms']);

Route::any('/settings','ManagementController@settings')->middleware(['auth', 'permission:update settings'])->name('settings');

Route::get('/import-shops',function(){
    if (($handle = fopen(base_path() . "/shop_details.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            /** 0 - serial, 1 - shop number, 2 - floor, 3 - block, 4 - size, 5 - price, 6 - service charge, 7 - tenant, 8 - phone */
            $shops = \Illuminate\Support\Facades\DB::table('shops');
            $tenants = \Illuminate\Support\Facades\DB::table('tenants');
            $floors = \Illuminate\Support\Facades\DB::table('floors');
            $blocks = \Illuminate\Support\Facades\DB::table('blocks');
            $shop_attributes = \Illuminate\Support\Facades\DB::table('shop_attributes');

            /*does not tenant exist*/
            if($tenants->where('phone','=',$data[8])->count() == 0){
                $tenant_id = $tenants->insertGetId(['name' => $data[7], 'phone'=> '0'.$data[8]]);
            }else{
                $tenant_id = $tenants->where('phone','=',$data[8])->first()->id;
            }

            /*does the floor exist*/
            if($floors->where(['project_id' => 1, 'name' => $data[2]])->count() == 0){
                $floor_id = $floors->insertGetId(['project_id' => 1, 'name' => $data[2]]);
            }else{
                $floor_id = $floors->where(['project_id' => 1, 'name' => $data[2]])->first()->id;
            }

            /*does the block exist*/
            if($blocks->where(['floor_id' => $floor_id, 'name' => $data[3]])->count() == 0){
                $block_id = $blocks->insertGetId(['project_id' => 1, 'floor_id' => $floor_id, 'name' => $data[3]]);
            }else{
                $block_id = $blocks->where(['floor_id' => $floor_id, 'name' => $data[3]])->first()->id;
            }

            /*does the shop exist*/
            if($shops->where(['project_id' => 1, 'shop_no' => $data[1]])->count() == 0){
                $shop_id = $shops->insertGetId([
                   'project_id' => 1,
                    'floor_id' => $floor_id,
                    'block_id' => $block_id,
                    'shop_no' => $data[1],
                    'tenant_id' => $tenant_id,
                    'price' => $data[5],
                    'service_charge' => $data[6],
                    'size' => $data[4],
                    'type' => 'sold'
                ]);
                $shop_attributes->insert([
                   ['shop_id' => $shop_id, 'attribute_name' => 'tenant', 'attribute_value' => $tenant_id, 'updated_by' => 1],
                   ['shop_id' => $shop_id, 'attribute_name' => 'price', 'attribute_value' => $data[5], 'updated_by' => 1],
                   ['shop_id' => $shop_id, 'attribute_name' => 'service_charge', 'attribute_value' => $data[6], 'updated_by' => 1],
                ]);
            }



        }
        fclose($handle);
    }
});
/*Route::get('/add-revenue',function(){
    $shops = \Illuminate\Support\Facades\DB::table('shops')
        ->join('floors', 'shops.floor_id', '=', 'floors.id')
        ->join('blocks','shops.block_id','=','blocks.id')
        ->join('tenants','shops.tenant_id','=','tenants.id')
        ->select('shops.*','floors.name as floor','blocks.name as block','tenants.id as tenant_id','tenants.name as tenant_name','tenants.phone as tenant_phone' )->get();
    $rev = \Illuminate\Support\Facades\DB::table('revenues');
    foreach($shops as $s){
        $rev->insert([
            'shop_id' => $s->id,
            'shop_no' => $s->shop_no,
            'project' => 'Saidpur Plaza',
            'floor' => $s->floor,
            'block' => $s->block,
            'size' => $s->size,
            'revenue_type' => 'service charge',
            'amount' => $s->service_charge,
            'due_date' => '2018-10-01',
            'tenant_id' => $s->tenant_id,
            'tenant_name' => $s->tenant_name,
            'tenant_phone' => $s->tenant_phone

        ]);
    }
});*/
/*Route::get('/create-roles',function(){
    \App\User::create([
        'name' => "Jafor",
        'username' => "top",
        'email' => "2jafor@gmail.com",
        'password' => \Illuminate\Support\Facades\Hash::make('topU$er'),
    ]);\App\User::create([
        'name' => "Admin",
        'username' => "admin",
        'password' => \Illuminate\Support\Facades\Hash::make('Adm1n'),
    ]);\App\User::create([
        'name' => "Manager X",
        'username' => "manager",
        'password' => \Illuminate\Support\Facades\Hash::make('manager123'),
    ]);\App\User::create([
        'name' => "Staff X",
        'username' => "staff",
        'password' => \Illuminate\Support\Facades\Hash::make('staff123'),
    ]);
    $user = \App\User::find(2);
    $user->assignRole('admin');
    $user = \App\User::find(3);
    $user->assignRole('manager');
    $user = \App\User::find(4);
    $user->assignRole('staff');
});*/

/*Route::get("/test-sms",function(){
   $sms = new App\MyTenantSMS\GreenWeb\GreenWeb();
   $sms->send(['01951669421','01951'],"welcome to our new tenant management system.");
});*/

/*Route::get("/add-permissions",function(){
   Spatie\Permission\Models\Permission::create(['name' => 'create deed']);
   Spatie\Permission\Models\Permission::create(['name' => 'create invoice']);
   Spatie\Permission\Models\Permission::create(['name' => 'manage tenants']);
   Spatie\Permission\Models\Permission::create(['name' => 'manage shops']);
   Spatie\Permission\Models\Permission::create(['name' => 'manage shops']);
});*/


