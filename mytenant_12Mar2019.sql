/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 10.1.34-MariaDB : Database - mytenant
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mytenant` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mytenant`;

/*Table structure for table `blocks` */

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `blocks` */

insert  into `blocks`(`id`,`project_id`,`floor_id`,`name`) values 
(1,1,1,'H'),
(2,1,1,'B '),
(3,1,1,'A');

/*Table structure for table `floors` */

DROP TABLE IF EXISTS `floors`;

CREATE TABLE `floors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `floors` */

insert  into `floors`(`id`,`project_id`,`name`) values 
(1,1,'G.F');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2018_09_02_092532_create_permission_tables',2),
(4,'2018_09_02_095344_create_column_is_active_in_table_users',3),
(6,'2018_09_02_095344_create_column_username_in_table_users',4),
(7,'2018_09_04_174032_create_table_tenants',5),
(8,'2018_09_04_174254_create_table_shops',5),
(9,'2018_09_04_175049_create_table_shop_attributes',5),
(10,'2018_09_04_175730_create_table_revenues',6),
(11,'2018_09_05_133759_craete_table_sales',7),
(12,'2018_09_05_141627_craete_table_sales_installments',7),
(13,'2018_09_05_153344_craete_table_settings',7),
(14,'2018_09_05_153700_craete_table_projects',7),
(15,'2018_09_05_153954_craete_table_floors',7),
(16,'2018_09_05_154059_craete_table_blocks',7),
(17,'2018_09_07_131051_make_columns_nullable',8),
(18,'2018_09_07_171553_make_column_nullable',9),
(19,'2018_09_28_152436_make_columns_nullable',10),
(21,'2018_10_04_162455_create_col_invoice_id_in_table_revenues',11),
(22,'2018_10_07_162139_create_column_block_in_table_revenues',12),
(23,'2018_10_13_085811_create_column_project_id_in_table_revenues',13),
(25,'2018_11_17_144140_drop_table_settings',14),
(26,'2018_11_18_050845_create_columns_in_sales_table',15),
(27,'2018_11_30_144727_create_table_ownership_transfers',16),
(28,'2018_11_30_145235_create_column_transfer_id_in_table_ownership_transfers',17),
(29,'2018_12_01_165309_make_transfer_id_nullable_in_revenues',18);

/*Table structure for table `model_has_permissions` */

DROP TABLE IF EXISTS `model_has_permissions`;

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_permissions` */

/*Table structure for table `model_has_roles` */

DROP TABLE IF EXISTS `model_has_roles`;

CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_roles` */

insert  into `model_has_roles`(`role_id`,`model_type`,`model_id`) values 
(1,'App\\User',1),
(2,'App\\User',4),
(2,'App\\User',5),
(2,'App\\User',7),
(2,'App\\User',9),
(2,'App\\User',10),
(3,'App\\User',3),
(3,'App\\User',6),
(3,'App\\User',8),
(4,'App\\User',2);

/*Table structure for table `ownership_transfers` */

DROP TABLE IF EXISTS `ownership_transfers`;

CREATE TABLE `ownership_transfers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `from_owner` int(11) NOT NULL,
  `to_owner` int(11) NOT NULL,
  `amount` double NOT NULL,
  `transfer_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `ownership_transfers` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects` */

insert  into `projects`(`id`,`name`,`address`) values 
(1,'Saidpur Plaza','Saidpur');

/*Table structure for table `revenues` */

DROP TABLE IF EXISTS `revenues`;

CREATE TABLE `revenues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_id` int(11) NOT NULL,
  `shop_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `project` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `block` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revenue_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `due_date` date NOT NULL,
  `collection_date` date DEFAULT NULL,
  `tenant_id` int(11) NOT NULL,
  `tenant_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenant_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collected_by` int(11) DEFAULT NULL,
  `invoice_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `revenues` */

insert  into `revenues`(`id`,`project_id`,`shop_id`,`shop_no`,`sale_id`,`transfer_id`,`project`,`floor`,`block`,`size`,`revenue_type`,`amount`,`due_date`,`collection_date`,`tenant_id`,`tenant_name`,`tenant_phone`,`collected_by`,`invoice_id`) values 
(1,'1',1,'13',NULL,NULL,'Saidpur Plaza','G.F','H','120','service charge',500.00,'2019-03-01',NULL,1,'Mst.Zobeda Begum','01722319720',NULL,NULL),
(2,'1',2,'16',NULL,NULL,'Saidpur Plaza','G.F','B ','120','service charge',500.00,'2019-03-01',NULL,2,'Fatima Rahman','01715650939',NULL,NULL),
(3,'1',3,'3',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',500.00,'2019-03-01',NULL,3,'Mahabub Alam','01715271045',NULL,NULL),
(4,'1',4,'4',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',500.00,'2019-03-01',NULL,4,'Hafizul Islam','01712596724',NULL,NULL),
(5,'1',5,'5',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',500.00,'2019-03-01',NULL,5,'Hazi. Md. Sardar Ahmed','01715597793',NULL,NULL),
(6,'1',6,'6',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',600.00,'2019-03-01',NULL,6,'Mst. Sanjana Samia','01717850031',NULL,NULL),
(7,'1',7,'7',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',600.00,'2019-03-01',NULL,7,'Saiful Islam','01761189533',NULL,NULL),
(8,'1',8,'8',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',600.00,'2019-03-01',NULL,8,'Mst. Farida Khatun','01686569008',NULL,NULL),
(9,'1',9,'9',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',700.00,'2019-03-01',NULL,9,'Md. Nazrul Islam Royel','01717136653',NULL,NULL),
(10,'1',10,'10',NULL,NULL,'Saidpur Plaza','G.F','A','120','service charge',500.00,'2019-03-01',NULL,10,'Bushra','01716610364',NULL,NULL);

/*Table structure for table `role_has_permissions` */

DROP TABLE IF EXISTS `role_has_permissions`;

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_has_permissions` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`guard_name`,`created_at`,`updated_at`) values 
(1,'super_admin','web','2018-09-02 09:51:09','2018-09-02 09:51:09'),
(2,'staff','web','2018-09-02 09:51:09','2018-09-02 09:51:09'),
(3,'manager','web','2018-09-02 09:51:09','2018-09-02 09:51:09'),
(4,'admin','web','2018-09-02 09:51:09','2018-09-02 09:51:09');

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `down_payment` double(8,2) NOT NULL,
  `no_of_installments` int(11) NOT NULL,
  `installment_interval` int(11) NOT NULL,
  `sale_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sales` */

/*Table structure for table `sales_installments` */

DROP TABLE IF EXISTS `sales_installments`;

CREATE TABLE `sales_installments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `due_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sales_installments` */

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `settings_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `settings_label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`settings_name`,`settings_label`,`value`) values 
(1,'service_charge_advance','Advance service charge (months)','10'),
(2,'transfer_fee','Transfer fee (%)','10');

/*Table structure for table `shop_attributes` */

DROP TABLE IF EXISTS `shop_attributes`;

CREATE TABLE `shop_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `attribute_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `shop_attributes` */

insert  into `shop_attributes`(`id`,`shop_id`,`attribute_name`,`attribute_value`,`updated_at`,`updated_by`) values 
(1,1,'tenant','1','2019-03-10 13:02:14',1),
(2,1,'price','300000','2019-03-10 13:02:14',1),
(3,1,'service_charge','500','2019-03-10 13:02:14',1),
(4,2,'tenant','2','2019-03-10 13:02:14',1),
(5,2,'price','300000','2019-03-10 13:02:14',1),
(6,2,'service_charge','500','2019-03-10 13:02:14',1),
(7,3,'tenant','3','2019-03-10 13:02:14',1),
(8,3,'price','300000','2019-03-10 13:02:14',1),
(9,3,'service_charge','500','2019-03-10 13:02:14',1),
(10,4,'tenant','4','2019-03-10 13:02:15',1),
(11,4,'price','325000','2019-03-10 13:02:15',1),
(12,4,'service_charge','500','2019-03-10 13:02:15',1),
(13,5,'tenant','5','2019-03-10 13:02:15',1),
(14,5,'price','265000','2019-03-10 13:02:15',1),
(15,5,'service_charge','500','2019-03-10 13:02:15',1),
(16,6,'tenant','6','2019-03-10 13:02:15',1),
(17,6,'price','300000','2019-03-10 13:02:15',1),
(18,6,'service_charge','600','2019-03-10 13:02:15',1),
(19,7,'tenant','7','2019-03-10 13:02:15',1),
(20,7,'price','300000','2019-03-10 13:02:15',1),
(21,7,'service_charge','600','2019-03-10 13:02:15',1),
(22,8,'tenant','8','2019-03-10 13:02:15',1),
(23,8,'price','300000','2019-03-10 13:02:15',1),
(24,8,'service_charge','600','2019-03-10 13:02:15',1),
(25,9,'tenant','9','2019-03-10 13:02:15',1),
(26,9,'price','300000','2019-03-10 13:02:15',1),
(27,9,'service_charge','700','2019-03-10 13:02:15',1),
(28,10,'tenant','10','2019-03-10 13:02:15',1),
(29,10,'price','300000','2019-03-10 13:02:15',1),
(30,10,'service_charge','500','2019-03-10 13:02:15',1);

/*Table structure for table `shops` */

DROP TABLE IF EXISTS `shops`;

CREATE TABLE `shops` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `shop_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenant_id` int(11) DEFAULT NULL,
  `rent` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `service_charge` double DEFAULT NULL,
  `size` int(11) NOT NULL,
  `disable_bulk_update` tinyint(1) DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `shops` */

insert  into `shops`(`id`,`project_id`,`floor_id`,`block_id`,`shop_no`,`tenant_id`,`rent`,`price`,`service_charge`,`size`,`disable_bulk_update`,`type`) values 
(1,1,1,1,'13',1,NULL,300000,500,120,NULL,'sold'),
(2,1,1,2,'16',2,NULL,300000,500,120,NULL,'sold'),
(3,1,1,3,'3',3,NULL,300000,500,120,NULL,'sold'),
(4,1,1,3,'4',4,NULL,325000,500,120,NULL,'sold'),
(5,1,1,3,'5',5,NULL,265000,500,120,NULL,'sold'),
(6,1,1,3,'6',6,NULL,300000,600,120,NULL,'sold'),
(7,1,1,3,'7',7,NULL,300000,600,120,NULL,'sold'),
(8,1,1,3,'8',8,NULL,300000,600,120,NULL,'sold'),
(9,1,1,3,'9',9,NULL,300000,700,120,NULL,'sold'),
(10,1,1,3,'10',10,NULL,300000,500,120,NULL,'sold');

/*Table structure for table `tenants` */

DROP TABLE IF EXISTS `tenants`;

CREATE TABLE `tenants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenants_phone_unique` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tenants` */

insert  into `tenants`(`id`,`name`,`phone`) values 
(1,'Mst.Zobeda Begum','01722319720'),
(2,'Fatima Rahman','01715650939'),
(3,'Mahabub Alam','01715271045'),
(4,'Hafizul Islam','01712596724'),
(5,'Hazi. Md. Sardar Ahmed','01715597793'),
(6,'Mst. Sanjana Samia','01717850031'),
(7,'Saiful Islam','01761189533'),
(8,'Mst. Farida Khatun','01686569008'),
(9,'Md. Nazrul Islam Royel','01717136653'),
(10,'Bushra','01716610364');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`is_active`,`username`) values 
(1,'Jafor','2jafor@gmail.com','$2y$10$KMuASacHXscpubfmNlnw8ORYVIQ2bYMkeo.OE/t81ubFpaRC1.m2e','rdsmd3TUqCN2IFSdTy1XQd7TATdZmXvvPP3J6mZqrA8GDMAmgLrG14yUJMaY','2018-09-02 10:32:39','2019-03-10 06:41:12',1,'top'),
(2,'Admin',NULL,'$2y$10$g9rMUyDV5wTat0sJ2cfSIO.S49XrrtnrGUq/ztxVLbxpqDfhmwc5u',NULL,'2018-09-02 10:32:39','2018-09-02 10:32:39',1,'admin'),
(3,'Manager X',NULL,'$2y$10$RUxcJ6h6GKhWciFfCJYkFu7IeFv8cjX9MQqaOezSYwy4xGsICHZtO','NX383f56cfXIzPC9AsIsKkGg6tftEiyklBHcPoUrr3XhN5teoMfQw1rIalTV','2018-09-02 10:32:39','2019-03-10 07:04:31',1,'manager'),
(4,'Staff X',NULL,'$2y$10$rmAXu12g9N4yCE/FejI0WO.6LgEf2JgBLvBGUNGmSdTMmY9plSx9q','eS3NIKRBXOM3pfuPLYraiiIEio6l1kiDtEWpfwmUQrlFnjoKfdnHIwLhAG12','2018-09-02 10:32:40','2019-03-10 06:41:56',1,'staff'),
(5,'mamunul haq',NULL,'$2y$10$lwZLT7u2X4yLEDtCto3N/un82gFwySMVTK41r9Ryv0NcJcoMyE3P6','fJcV8SQkKpPhYffteopFM5YYmF6QSf0CPhVXKKcalXPcJ9Zkl7fZCMuZRgiV','2019-01-13 11:20:52','2019-03-10 06:27:52',1,'mamun'),
(6,'jomuna',NULL,'$2y$10$06nl.LCYFf.l0Ygn8z1O3OuC/4E0Qvi63TgeBUWgYWhle1l6YBvyS','9o3Ye0rCEnT04uIHQf9ZTBVUN539xli9raDsBWRWroTFKcoGJeG5oh1elC5E','2019-01-13 11:21:34','2019-01-13 11:21:34',1,'jomuna'),
(7,'Abir ahad russel',NULL,'$2y$10$thY395DW079sP6r6t/WGheNU/fjxe.fONvB0j8ccDMLwpsIvs7Q8G','FrTrFvopj0tiLP8CbKpNpprB09yXUcCNCehaqLYkx2ZtgdrQe4FCgmEolejd','2019-01-13 11:23:44','2019-01-14 17:09:54',1,'abir'),
(8,'roni biswas',NULL,'$2y$10$nCQkIpeB8xFChk6jMAFmG.M1CmKkMAHfOOpjk19YHDSwhnxhGIRg.','CyOFPNu8b44wstxTyTOvSCrFdyRKLbbEVZV5nTwpfjeonbKDXvDA3iE2bTrh','2019-01-13 11:26:24','2019-01-13 11:26:24',1,'roni'),
(9,'riaz',NULL,'$2y$10$m02HZcPF9ZWHh4UM95pJ7Of2gn9vrIj17hx4O9T15TBuFCCNoq2nS',NULL,'2019-01-13 11:27:38','2019-01-13 11:27:38',1,'riaz'),
(10,'salman md',NULL,'$2y$10$V5vGAIDepFpE2WdOTGm/subGRje.hBoV2EhCxm0ko6YgZpaOf5hYG',NULL,'2019-01-13 12:15:49','2019-01-13 12:15:49',0,'salman');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
