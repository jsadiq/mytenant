@extends("layouts.app")
@section('title') Collect service charge @endsection
@section('content')
    @if($invoice_id != '')
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="height: 900px">
                    <div class="card-body">
                        <iframe style="border: none; width: 100%; height: 100%;" src="{{route('invoice',['invoice_id'=>$invoice_id])}}"></iframe>
                    </div>
                </div>
            </div>
        </div>
    @else

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <select name="project" class="form-control select2 custom-select" data-bind="event: {change: projectSelected}" tabindex="1">
                                <option value="" selected>select project</option>
                                @foreach($projects as $project)
                                    <option value="{{$project->id}}">{{$project->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6" data-bind="visible: shops().length > 0">
                            <select name="shop" class="form-control select2 custom-select"
                                    data-bind="options: shops,
                                            optionsText: 'shop_no',
                                            value: selected_shop,
                                            optionsCaption: 'select shop no.'" tabindex="2">

                            </select>
                        </div>
                    </div>
                    <div class="row" data-bind="visible: selected_shop_data().tenant.name != ''">
                        <div class="col-md-7">
                            <h3>Tenant: <span data-bind="text: selected_shop_data().tenant.name"></span> (<span data-bind="text: selected_shop_data().tenant.phone"></span>)</h3>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row" data-bind="visible: selected_shop_data().tenant.name != ''">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Overdue payments</div>
                <div class="card-body">
                    <p data-bind="visible: selected_shop_data().overdue_service_charge.length == 0">No payment overdue</p>
                    <table data-bind="visible: selected_shop_data().overdue_service_charge.length != 0" class="table">
                        <thead>
                        <tr>
                            <th>Due date</th>
                            <th>Amount</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: selected_shop_data().overdue_service_charge">
                            <tr>
                                <td data-bind="text: due_date_formatted"></td>
                                <td data-bind="text: amount.toFixed(2) + ' tk.'"></td>
                                <td>
                                    <button class="btn btn-primary btn-sm" data-bind="click: addPaymentToInvoice">add to invoice</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Invoice</div>
                <div class="card-body">
                    <p data-bind="visible: selected_payments().length == 0">No payment added</p>
                    <table data-bind="visible: selected_payments().length != 0" class="table">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th class="text-right">Amount</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: selected_payments">
                            <tr>
                                <td data-bind="text: 'Service charge - ' + month"></td>
                                <td class="text-right"><span data-bind="text: amount.toFixed(2) + ' tk.'"></span></td>
                                <td><button class="btn btn-sm btn-danger" data-bind="click: remPaymentFromInvoice">remove</button></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-right"></td>
                                <td class="text-right"><h3 data-bind="text:  'Total:   ' + total_amount().toFixed(2) + ' tk.'"></h3></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="text-left" colspan="3"><button data-bind="click: collect_payment" class="btn btn-default">Collect payment</button></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="collect_payment" method="POST" action="{{route('collect-service-charge')}}">
        @csrf
        <input type="hidden" name="data">
    </form>

    @endif
@endsection
@section('script')
    <script>
        var viewModel = function () {
            self = this;
            self.selected_shop = ko.observable('');
            self.selected_shop_data = ko.observable({
                tenant:{
                    name: '', phone: ''
                },
                overdue_service_charge:[]
            });
            self.selected_payments = ko.observableArray([]);
            self.shops = ko.observableArray([]);
            projectSelected = function(){
                var proj_id = $("select[name=project]").val();
                self.reset();
                self.shops([]);
                if(proj_id == '') return;
                $.ajax( app_path + '/api/getShops',{
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'post',
                    data:{
                        project_id: proj_id,
                        shop_type: 'sold'
                    },
                    success: function(data){

                        if(data.success != undefined && data.shops.length > 1){
                            self.shops(data.shops);
                        }
                    }
                })
            };
            self.selected_shop.subscribe(function(shop){
                if(shop == undefined || shop == '') return;
                $.ajax( app_path + '/api/getShopData',{
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'post',
                    data:{
                        shop_id: shop.id,
                        tenant_id: shop.tenant_id
                    },
                    success: function(data){
                        if(data.success != undefined){
                            self.selected_shop_data({
                                service_charge: data.shop.service_charge,
                                tenant: data.tenant,
                                overdue_service_charge: data.overdue_service_charge
                            });
                            self.selected_payments([]);
                        }
                    }
                })
            });
            self.addPaymentToInvoice = function(payment){
                var key = moment(payment.due_date).format("YYYY_MM");
                for(var i = 0; i < self.selected_payments().length; i++){
                    if(self.selected_payments()[i].key == key){
                        alert("Already added.");
                        return;
                    }
                }
                self.selected_payments.push({
                    key: key, month: moment(payment.due_date).format("MMMM YYYY"), amount: payment.amount, overdue_id: payment.id
                });
            };
            self.remPaymentFromInvoice = function(){
                self.selected_payments.remove(this);
            };
            self.total_amount = ko.computed(function(){
               var total = 0;
               for(var i=0; i < self.selected_payments().length; i++){
                    total += self.selected_payments()[i].amount;
               }
                return total;
            });
            self.collect_payment = function(){
                var data = {
                    shop_id: self.selected_shop().id,
                    payments: self.selected_payments()
                };
                $("form#collect_payment input[name=data]").val(JSON.stringify(data)).parent().submit();
            };
            self.reset = function(){
                self.selected_shop('');
                self.selected_shop_data({
                    tenant:{
                        name: '', phone: ''
                    },
                    overdue_service_charge:[]
                });
                self.selected_payments([]);
            }
        };
        ko.applyBindings(viewModel);
        $(".select2").select2();
    </script>
@endsection