@extends("layouts.app")
@section('title') Sale Shop @endsection
@section('content')
    @if(isset($invoice_id))
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="height: 900px">
                    <div class="card-body">
                        <iframe style="border: none; width: 100%; height: 100%;" src="{{route('invoice',['invoice_id'=>$invoice_id])}}"></iframe>
                    </div>
                </div>
            </div>
        </div>
    @else
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-3  text-right">
                            <label class="col-form-label">Project:</label>
                        </div>
                        <div class="col-md-5">
                            <select name="project" class="form-control select2 custom-select" data-bind="event: {change: projectSelected}" tabindex="1">
                                <option value="" selected>select project</option>
                                @foreach($projects as $project)
                                    <option value="{{$project->id}}">{{$project->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" data-bind="visible: shops().length > 0">
                        <div class="col-md-3 text-right">
                            <label class="col-form-label">Shop:</label>
                        </div>
                        <div class="col-md-5">
                            <select name="shop" class="form-control select2 custom-select"
                                    data-bind="options: shops,
                                            optionsText: 'shop_no',
                                            value: selected_shop,
                                            optionsCaption: 'select shop no.'" tabindex="2">

                            </select>
                        </div>
                    </div>
                    <div class="row" data-bind="visible: selected_shop() != null ">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-3 text-right">
                                    <label class="col-form-label">Price:</label>
                                </div>
                                <div class="col-md-5"><label class="col-form-label" data-bind="text:selected_shop_price"></label> tk.</div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-3  text-right">
                                    <label class="col-form-label">Advance service charge:</label>
                                </div>
                                <div class="col-md-5"><label class="col-form-label" data-bind="text:advance_service_charge"></label> tk.</div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-3 text-right">
                                    <label class="col-form-label">Sell to:</label>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control select2 custom-select" name="tenant">
                                        <option value="" selected>select tenant</option>
                                        @foreach($tenants as $tenant)
                                            <option value="{{$tenant->id}}">{{$tenant->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-3 text-right">
                                    <label class="col-form-label">Down payment:</label>
                                </div>
                                <div class="col-md-5">
                                    <input class="form-control" data-bind="textInput: payments().down_payment, event: {keyup: computeTotal}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-3 text-right">
                                    <label class="col-form-label">No. of Installments:</label>
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" type="number" min="0" style="width: 10%; float: left" data-bind="textInput: payments().no_of_installments">
                                    <span class="form-control" style="border: none; float: left;width: 26%">Collect installment in every</span>
                                    <input type="number" min="1" style="float: left; width: 10%"  class="form-control" data-bind="textInput: payments().installment_interval">
                                    <span style="padding: 6px 12px; float: left;">months</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right" data-bind="visible: total_amount() != null && !isNaN(total_amount())">
                            <h2>Total: <span data-bind="text:total_amount()"></span> tk.</h2>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-12 text-right">
                                    <button class="btn btn-default" data-bind="click:create_invoice">Create invoice</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="advance" value="{{$advance}}">
    <form id="collect_payment" method="POST" action="{{route('sale')}}">
        @csrf
        <input type="hidden" name="data">
    </form>
    @endif
@endsection
@section('script')
    <script language="javascript">
       $(".select2").select2();
        var viewModel = function(){
            var self = this;
            self.selected_shop = ko.observable(null);
            self.shops = ko.observableArray([]);
            self.payments = ko.observable({'down_payment': '', no_of_installments:'', installment_interval:''});
            self.projectSelected = function(){
                var proj_id = $("select[name=project]").val();
                self.reset(true);
                if(proj_id == '') return;
                $.ajax( app_path + '/api/getShops',{
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'post',
                    data:{
                        project_id: proj_id,
                        shop_type: 'for_sale'
                    },
                    success: function(data){
                        if(data.success != undefined && data.shops.length > 0){
                            self.shops(data.shops);
                        }else{
                            if(data.shops.length == 0){
                                alert("No shop for sell.")
                            }
                        }

                    }
                })
            };
            self.reset = function(reset_shops){
                if(reset_shops){
                    self.shops([]);
                }
                self.selected_shop(null);
                self.payments = ko.observable({'down_payment': '', no_of_installments:'', installment_interval:''});
                self.total_amount(null);
            };
            self.selected_shop_price = ko.computed(function(){
               if(self.selected_shop() != null){
                   return self.selected_shop().price.toFixed(2);
               }
                return '';
            });
            self.advance_service_charge = ko.computed(function(){
                if(self.selected_shop() != null){
                    return (self.selected_shop().service_charge * $("#advance").val()).toFixed(2);
                }
                return '';
            });
            self.create_invoice = function(){
                var data = {
                    shop_id: self.selected_shop().id,
                    tenant_id: $("select[name=tenant]").val(),
                    payments: self.payments()
                };
                $("form#collect_payment input[name=data]").val(JSON.stringify(data)).parent().submit();
            };
            self.total_amount = ko.observable(null);
            self.computeTotal = function(){
                if(isNaN(self.payments().down_payment)){
                    toastr.error("Enter a correct down payment amount");
                }else{
                    self.total_amount((self.selected_shop().service_charge * $("#advance").val() + parseInt(self.payments().down_payment)).toFixed(2));
                }
            }
        };
        ko.applyBindings(viewModel);
    </script>
@endsection