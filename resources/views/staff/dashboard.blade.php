@extends("layouts.app")
@section('title') Dashboard @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Overdue Collections</div>
                <div class="card-body">
                    <table id="tbl" class="table table-striped table-bordered compact">
                        <thead>
                            <tr>
                                <th>Project</th>
                                <th>Shop no.</th>
                                <th>Due on</th>
                                <th>Collection type</th>
                                <th>Tenant</th>
                                <th>Phone</th>
                                <th>Amount</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($payments as $p)
                            <tr>
                                <td>{{$p->project}}</td>
                                <td>{{$p->shop_no}}</td>
                                <td>{{$p->due_date}}</td>
                                <td>{{$p->revenue_type}}</td>
                                <td>{{$p->tenant_name}}</td>
                                <td>{{$p->tenant_phone}}</td>
                                <td>{{$p->amount}}</td>
                                <td>
                                    <button
                                            data-toggle="modal"
                                            data-target="#frm"
                                            class="btn btn-sm btn-secondary"
                                            data-name="{{$p->tenant_name}}"
                                            data-id="{{$p->id}}"
                                            data-amount="{{$p->amount}}"
                                            data-type="{{$p->revenue_type}}">collect</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--confirmation dialog--}}
    <div class="modal fade" id="frm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Collect Revenue</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <form class="form-horizontal" id="collectionFrm" method="post" action="{{route('collect-revenue')}}">
                                    @csrf
                                    <input type="hidden" id="revId" name="id">
                                    <div class="card-body">
                                        <table class="table" style="font-size: 24px">
                                            <tr>
                                                <td class="text-right">Collect:</td>
                                                <td><b><span id="amount"></span></b> tk</td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">From:</td>
                                                <td><b><span id="tenant"></span></b></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">As:</td>
                                                <td style="text-transform: capitalize"><b><span id="rev_type"></span></b></td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="$('#collectionFrm').submit()">Create Invoice</button>
                </div>
            </div>
        </div>
    </div>

    {{--invoice--}}
    @if(isset($invoice_id))
    <div id="invoiceId" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="height: 500px" >
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel2">Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe style="border: none; width: 100%; height: 100%;" src="{{route('invoice',['invoice_id'=>$invoice_id])}}"></iframe>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script>
        $("#tbl").DataTable({
            paging: false
        });
        $('#frm').on('show.bs.modal', function (e) {
            $("#revId").val($(e.relatedTarget).data('id'));
            $("#tenant").html($(e.relatedTarget).data('name'));
            $("#amount").html($(e.relatedTarget).data('amount'));
            $("#rev_type").html($(e.relatedTarget).data('type'));
        });
        $(document).ready(function(){
            $('#invoiceId').modal('show');
        })
    </script>
@endsection