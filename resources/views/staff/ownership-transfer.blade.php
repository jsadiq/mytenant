@extends("layouts.app")
@section('title') Collect Installment @endsection
@section('content')
    @if(isset($invoice_id))
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="height: 900px">
                    <div class="card-body">
                        <iframe style="border: none; width: 100%; height: 100%;" src="{{route('invoice',['invoice_id'=>$invoice_id])}}"></iframe>
                    </div>
                </div>
            </div>
        </div>
    @else
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-3  text-right">
                            <label class="col-form-label">Project:</label>
                        </div>
                        <div class="col-md-5">
                            <select name="project" class="form-control select2 custom-select" data-bind="event: {change: projectSelected}" tabindex="1">
                                <option value="" selected>select project</option>
                                @foreach($projects as $project)
                                    <option value="{{$project->id}}">{{$project->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" data-bind="visible: shops().length > 0">
                        <div class="col-md-3 text-right">
                            <label class="col-form-label">Shop:</label>
                        </div>
                        <div class="col-md-5">
                            <select name="shop" class="form-control select2 custom-select"
                                    data-bind="options: shops,
                                            optionsText: 'shop_no',
                                            value: selected_shop,
                                            optionsCaption: 'select shop no.'" tabindex="2">

                            </select>
                        </div>
                    </div>
                    <div class="form-group row" data-bind="visible: selected_shop() != null ">
                        <div class="col-md-3 text-right">
                            <label class="col-form-label">Current Tenant:</label>
                        </div>
                        <div class="col-md-5">
                            <label class="col-form-label" data-bind="text: selected_shop_data().tenant.name + ' (' + selected_shop_data().tenant.phone + ')'"></label>
                        </div>
                    </div>
                    <div class="form-group row" data-bind="visible: selected_shop_data().potential_buyers != null">
                        <div class="col-md-3 text-right">
                            <label class="col-form-label">Transfer To:</label>
                        </div>
                        <div class="col-md-5">
                            <select name="buyers" class="form-control select2 custom-select"
                                    data-bind="options: selected_shop_data().potential_buyers,
                                            optionsText: 'name',
                                            value: transfer_to,
                                            optionsCaption: 'select tenant.'" tabindex="2">

                            </select>
                        </div>
                    </div>
                    <div class="form-group row" data-bind="visible: transfer_to() != null ">
                        <div class="col-md-3 text-right">
                            <label class="col-form-label">Amount:</label>
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" data-bind="textInput:transfer_amount">
                        </div>
                    </div>
                    <div class="form-group row" data-bind="visible:!isNaN(transfer_amount()) && transfer_amount() != null && transfer_amount() != ''">
                        <div class="col-md-3 text-right">
                            <label class="col-form-label">Transfer Fee:</label>
                        </div>
                        <div class="col-md-5">
                            <label class="col-form-label" data-bind="text: transfer_fee"></label> tk.
                        </div>
                    </div>
                    <div class="form-group row" data-bind="visible:!isNaN(transfer_amount()) && transfer_amount() != null && transfer_amount() != ''">
                        <div class="col-md-3 text-right">
                            <label class="col-form-label"></label>
                        </div>
                        <div class="col-md-5">
                            <input type="hidden" id="transfer_fee" value="{{$transfer_fee}}">
                            <form id="collect_payment" method="POST" action="{{route('ownership-transfer')}}">
                                @csrf
                                <input type="hidden" name="shop" data-bind="textInput:(selected_shop() != null)?selected_shop().id:''">
                                <input type="hidden" name="transfer_to" data-bind="textInput:(transfer_to() != null)?transfer_to().id:''">
                                <input type="hidden" name="amount" data-bind="textInput:transfer_amount()">
                                <button class="btn btn-primary">Create Invoice</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
@section('script')
    <script language="javascript">
       $(".select2").select2();
        var viewModel = function(){
            var self = this;
            self.selected_shop = ko.observable(null);
            self.selected_shop_data = ko.observable({
                tenant:{
                    name: '', phone: ''
                },
                potential_buyers: null
            });
            self.transfer_to = ko.observable(null);
            self.transfer_amount = ko.observable(null);
            self.shops = ko.observableArray([]);
            self.projectSelected = function(){
                var proj_id = $("select[name=project]").val();
                self.reset(true);
                if(proj_id == '') return;
                $.ajax( app_path + '/api/getShops',{
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'post',
                    data:{
                        project_id: proj_id,
                        shop_type: 'sold'
                    },
                    success: function(data){
                        if(data.success != undefined && data.shops.length > 0){
                            self.shops(data.shops);
                        }
                    }
                })
            };
            self.selected_shop.subscribe(function(shop){
                if(shop == undefined || shop == '') return;
                $.ajax( app_path + '/api/getShopData',{
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'post',
                    data:{
                        shop_id: shop.id,
                        tenant_id: shop.tenant_id
                    },
                    success: function(data){
                        if(data.success != undefined){
                            self.selected_shop_data({
                                service_charge: data.shop.service_charge,
                                tenant: data.tenant,
                                potential_buyers: data.potential_buyers
                            });
                            self.transfer_to(null);
                            self.transfer_amount(null);
                        }else{
                            self.reset();
                        }
                    }
                })
            });
            self.reset = function(reset_shops){
                if(reset_shops){
                    self.shops([]);
                }
                self.selected_shop(null);
                self.selected_shop_data({
                    tenant:{
                        name: '', phone: ''
                    },
                    potential_buyers: null
                });
                self.transfer_to(null);
                self.transfer_amount(null);
            };

            self.transfer_fee = ko.computed(function(){
                var rate = $("#transfer_fee").val();
                var transfer_fee = self.transfer_amount() * rate / 100;
                if(!isNaN(transfer_fee)){
                    return transfer_fee.toFixed(2);
                }
                return '';
            });
        };
        ko.applyBindings(viewModel);
    </script>
@endsection