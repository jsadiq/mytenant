@extends("layouts.app")
@section('title')Clients @endsection
@section('content')
        <div class="row">
            <div class="col-md-12">
                <button data-toggle="modal" data-target="#add" class="btn btn-primary">Add a new client</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right" style="margin-bottom: 12px">
                <button  data-toggle="modal" data-target="#sms_form"  id="send_sms" class="btn btn-sm btn-primary" style="visibility: hidden;">Send SMS to selected tenants</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="tbl"  class="table table-striped table-bordered compact" style="width:100%">
                    <thead>
                    <tr>
                        <th class="text-center"><input type="checkbox" class="sel-all"></th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Shops</th>
                        <th>Overdue</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($tenants as $tenant)
                            <tr>
                                <td class="text-center"><input type="checkbox" class="sel-record" data-tenant-name="{{$tenant->name}}" data-tenant-phone="{{$tenant->phone}}"></td>
                                <td>{{$tenant->name}}</td>
                                <td>{{$tenant->phone}}</td>
                                <td>{{$tenant->shops}}</td>
                                <td style="text-align: right">{{number_format($tenant->due, 2)}}</td>
                                <td><button data-toggle="modal" data-target="#edit" class="btn btn-sm btn-secondary" data-name="{{$tenant->name}}" data-phone="{{$tenant->phone}}" data-id="{{$tenant->id}}" data-title="{{$tenant->title}}">Edit info</button> </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <form class="form-horizontal" id="editFrm" method="post" action="{{route('tenant-save')}}">
                                        @csrf
                                        <input type="hidden" id="editId" name="id">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title</label>
                                                <div class="col-sm-9">
                                                    <select name="title" id="titleEdit" class="form-control">
                                                        <option value="Mr.">Mr.</option>
                                                        <option value="Mrs.">Mrs.</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="name" class="form-control" id="nameEdit" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Phone</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="phone" class="form-control" id="phoneEdit"  required>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="$('#editFrm').submit()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <form class="form-horizontal" id="addFrm" method="post" action="{{route('tenant-save')}}">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title</label>
                                                <div class="col-sm-9">
                                                    <select name="title" class="form-control">
                                                        <option value="Mr.">Mr.</option>
                                                        <option value="Mrs.">Mrs.</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="name" class="form-control" id="" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Phone</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="phone" class="form-control" id=""  required>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="$('#addFrm').submit()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        @component("sms")
        @endcomponent
@endsection

@section('script')
    <script>
        $(".project-select").select2({placeholder: '---select a project---'});
        $("#tbl").DataTable({
            paging: false,
            columnDefs: [ {
                "targets": [0, 5],
                "sortable": false,
            },{
                "targets": [0,5],
                "searchable": false,
            } ],
            order: [[ 1, 'asc' ]],
        });
        $('#edit').on('show.bs.modal', function (e) {
            $("#nameEdit").val($(e.relatedTarget).data('name'));
            $("#phoneEdit").val($(e.relatedTarget).data('phone'));
            $("#editId").val($(e.relatedTarget).data('id'));
            $("#titleEdit").val($(e.relatedTarget).data('title'));
        });
        $(".sel-all").click(function(){
            $(".sel-record").prop("checked",$(this).is(":checked"));
            if($(this).is(":checked")){
                $("#send_sms").css('visibility','visible');
            }else{
                $("#send_sms").css('visibility','hidden');
            }
        });
        $("body").on('click','.sel-record',function(){
            if($(".sel-record:checked").length > 0){
                $("#send_sms").css('visibility','visible');
            }else{
                $("#send_sms").css('visibility','hidden');
            }
        });
    </script>
@endsection