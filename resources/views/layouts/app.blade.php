<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">

    <title>My Tenant - @yield('title')</title>

</head>
<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper" data-sidebartype="full">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar" data-navbarbg="skin5">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
            <div class="navbar-header" data-logobg="skin5">
                <!-- This is for the sidebar toggle which is visible on mobile only -->
                <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <b class="logo-icon p-l-10" style="display: none">
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <img src="{{asset("images/logo-mini.png")}}" alt="homepage" class="light-logo">

                    </b>
                    <!-- Logo text -->
                        <span class="logo-text">
                             <!-- dark Logo text -->
                             <img src="{{asset("images/logo.png")}}" alt="homepage" class="light-logo"/>

                        </span>
                    <!-- Logo icon -->
                    <!-- <b class="logo-icon"> -->
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                    <!-- </b> -->
                    <!--End Logo icon -->
                </a>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Toggle which is visible on mobile only -->
                <!-- ============================================================== -->
                <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                   data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                   aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5" style="align-self: first baseline;">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-left mr-auto">
                    <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light"
                                                              href="javascript:void(0)" data-sidebartype="mini-sidebar"><i
                                    class="mdi mdi-menu font-24"></i></a></li>
                    <!-- ============================================================== -->
                </ul>
                <!-- ============================================================== -->
                <!-- Right side toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-right">
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown" style="color:white">
					<?php
					$user = auth()->user();
					echo "Welcome $user->username";
					?>
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href=""
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
                                    src="{{asset("images/user.jpg")}}" alt="user" class="rounded-circle" width="31"></a>

                        <div class="dropdown-menu dropdown-menu-right user-dd animated">
                            <a class="dropdown-item" href="{{ url('change-password') }}">
                                <i class="ti-user m-r-5 m-l-5"></i>
                                Change password
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="fa fa-power-off m-r-5 m-l-5"></i>
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                </ul>
            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar" data-sidebarbg="skin5">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav" class="p-t-30">
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('booking')}}" aria-expanded="false">
                            <i class="mdi mdi-home-floor-b"></i>
                            <span class="hide-menu">New Booking</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('deed')}}" aria-expanded="false">
                            <i class="mdi mdi-alpha-d-box"></i>
                            <span class="hide-menu">New Deed</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('ledgers',['ledger_type'=>'shop'])}}" aria-expanded="false">
                            <i class="mdi mdi-file-document-box-multiple-outline"></i>
                            <span class="hide-menu">Shop Ledgers</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('ledgers',['ledger_type'=>'service_charge'])}}" aria-expanded="false">
                            <i class="mdi mdi-calendar-text-outline"></i>
                            <span class="hide-menu">S.Charge Ledgers</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('ledgers',['ledger_type'=>'other'])}}" aria-expanded="false">
                            <i class="mdi mdi-playlist-plus"></i>
                            <span class="hide-menu">Other Ledgers</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('payment')}}" aria-expanded="false">
                            <i class="mdi mdi-currency-bdt"></i>
                            <span class="hide-menu">Collect Payment</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('overdue-report')}}" aria-expanded="false">
                            <i class="mdi mdi-format-list-checks"></i>
                            <span class="hide-menu">Overdue Report</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect sidebar-link" href="{{route('revenue-report')}}" aria-expanded="false">
                            <i class="mdi mdi-chart-areaspline"></i>
                            <span class="hide-menu">Revenue Report</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect" href="{{route('shop-list')}}" aria-expanded="false">
                            <i class="mdi mdi-home-group"></i>
                            <span class="hide-menu">Shops</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect" href="{{route('tenant-list')}}" aria-expanded="false">
                            <i class="mdi mdi-account-group"></i>
                            <span class="hide-menu">Clients</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect" href="{{route('create-other-ledger')}}" aria-expanded="false">
                            <i class="mdi mdi-calendar-plus"></i>
                            <span class="hide-menu">Create Other Ledger</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect" href="{{route('create-ledger-entries')}}" aria-expanded="false">
                            <i class="mdi mdi-note-plus"></i>
                            <span class="hide-menu">Create Ledger Entries</span>
                        </a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect" href="{{route('settings')}}" aria-expanded="false">
                            <i class="mdi mdi-settings"></i>
                            <span class="hide-menu">Settings</span>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        @if (session('error'))
            <div class="page-breadcrumb">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <?php echo session('error'); ?>
                    </div>
                </div>
            </div>
            </div>
        @endif
        @if (session('success'))
            <div class="page-breadcrumb">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
            </div>
        @endif
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">@yield('title')</h4>

                    {{--<div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>--}}
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            @yield('content')
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer text-center">
            All Rights Reserved by .... Designed and Developed by ....
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset("js/jquery-3.3.1.min.js")}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset("js/bootstrap.min.js")}}"></script>
<!--Menu sidebar -->
<script src="{{asset("js/sidebarmenu.js")}}"></script>
<!-- jquery ui -->
<script src="{{asset("js/jquery-ui.min.js")}}"></script>
<!--Custom JavaScript -->
<script src="{{asset("js/custom.min.js")}}"></script>

<script src="{{asset("js/knockout-min.js")}}"></script>
<script src="{{asset("js/select2.min.js")}}"></script>
<script src="{{asset("js/moment.min.js")}}"></script>
<script src="{{asset("js/toastr.min.js")}}"></script>
<script src="{{asset("js/datatables.min.js")}}"></script>

<script>
    var app_path = '{!! url('/') !!}';
</script>

@isset($success_msg)
    <script>
        toastr.success("{{$success}}");
    </script>
@endisset
@isset($error_msg)
<script>
    toastr.error("{{$error_msg}}");
</script>
@endisset

@yield('script')

</body>
</html>
