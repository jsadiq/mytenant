<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        /*font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;*/
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        /*border-top: 2px solid #eee;*/
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
    <script>
        window.print();
    </script>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="3">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{asset('images/sr_trading.gif')}}" style="">
                            </td>
                            
                            <td>
                                Invoice #: {{$invoice_id}}<br>
                                Created: {{date('d F Y, h:i A')}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="3">
                    <table>
                        <tr>
                            <td>
                                S.R. Trading <br>
								Suite # 1101, Concord Tower <br>
								113 Kazi Nazrul Islam Avenue <br>
								Dhaka-1217. <br>
								Tel: +88-02-9351259
                            </td>
                            
                            <td>
                                {{$payments[0]->tenant_name}}<br>
                                Tel: {{$payments[0]->tenant_phone}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Shop
                </td>
                <td style="text-align: left">
                    Description
                </td>
                
                <td style="text-align: right">
                    Amount
                </td>
            </tr>
            @php
                $amount = 0;
            @endphp
            @foreach($payments as $payment)
            <tr class="item">
                <td><?php echo str_replace(',Shop-','<br>Shop-',$payment->details); ?></td>
                <td style="text-align: left">
                    @switch($payment->revenue_type)
                        @case('service charge')
                            SERVICE CHARGE
                            @break
                        @endcase
                        @case('down payment')
                            DOWN PAYMENT
                            @break
                        @endcase
                        @case('installment')
                            INSTALLMENT
                            @break
                        @endcase
                        @case('transfer fee')
                            TRANSFER FEE
                            @break
                        @endcase
                        @case('shop price')
                            SHOP PRICE
                        @break
                        @endcase
                    @endswitch
                </td>

                <td style="text-align: right">
                    {{number_format($payment->amount,2)}} Tk.
                    @php
                    $amount += $payment->amount;
                    @endphp
                </td>
            </tr>
            @endforeach
            <tr class="total">
                <td colspan="2"></td>
                
                <td>
                   Total: {{number_format($amount,2)}} tk.
                </td>
            </tr>
        </table>
		<table style="margin-top: 80px">
			<tr>
				<td style="width: 30%; text-align: left">
					Prepared By
				</td>
				<td style="width: 30%; text-align: center">
					Received By
				</td>
				<td style="width: 30%; text-align: right">
					 Project Director
				</td>
			</tr>	
		</table>
    </div>
	
</body>
</html>
