<div class="modal fade" id="sms_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send SMS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" id="smsFrm" method="post">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">To</label>
                                        <div class="col-sm-9">
                                            <p id="smsto"></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">Others</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="others" id="other" style="width: 100%"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label"></label>
                                        <div class="custom-control custom-checkbox col-sm-9">
                                            <input type="checkbox" name="common_sms" value="yes" class="custom-control-input" id="common_sms">
                                            <label class="custom-control-label" for="common_sms">Common to All</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label">Message</label>
                                        <div class="col-sm-9">
                                            <textarea required name="message" style="width: 100%"></textarea>
                                            <small><strong id="charcount"></strong> characters remaining</small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label"></label>
                                        <div class="col-sm-9">
                                            <small><strong>Tokens:</strong> <br>
                                            client_title = Mr./Mrs.<br>
                                            client_name = client name <br>
                                            shop_price_due = Due shop price <br>
                                            service_charge_due = Service charge due <br>
                                            total_due = Total due
                                            </small>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-sm-3 text-right control-label col-form-label"></label>
                                        <div class="col-sm-9">
                                            <input type="submit" class="btn btn-success" value="send">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>