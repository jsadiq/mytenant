@extends("layouts.app")
@section('title') User Management @endsection
@section('content')
    <?php $permissions = Spatie\Permission\Models\Permission::all(); ?>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 text-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#add" >Create User</button>
            <br>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Permissions</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $u)
                            <tr>
                                <td>{{$u->name}}</td>
                                <td>{{$u->username}}</td>
                                <td>
                                    <?php $user =  App\User::where('id', $u->id)->first(); ?>
                                    <ul>
                                    @foreach($user->getPermissionNames() as $user_permission)
                                        <li>{{$user_permission}}</li>
                                    @endforeach
                                    </ul>
                                </td>
                                <td><?php echo ($u->is_active) ? "active" : "inactive"; ?></td>
                                <td>
                                    <button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#edit-{{$u->id}}">Edit</button>
                                    <div class="modal fade" id="edit-{{$u->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit User: {{$u->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form class="form-horizontal" id="" method="post" action="{{route('edit-user')}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="card">
                                                                        <input type="hidden" id="editId" name="id">
                                                                        <div class="card-body">
                                                                            <div class="form-group row">
                                                                                <label for="fname" class="col-sm-4 text-right control-label col-form-label">Name</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="name" class="form-control" id="" value="{{$u->name}}" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="fname" class="col-sm-4 text-right control-label col-form-label">Username</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="text" name="username" class="form-control" id="" value="{{$u->username}}" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="fname" class="col-sm-4 text-right control-label col-form-label">Password</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="password" name="password" class="form-control" id="" >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="fname" class="col-sm-4 text-right control-label col-form-label">Re-type Password</label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="password" name="password_confirmation" class="form-control" id="" >
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="fname" class="col-sm-4 text-right control-label col-form-label">Permissions</label>
                                                                                <div class="col-sm-8">
                                                                                    @foreach($permissions as $permission)
                                                                                    <div class="custom-control custom-checkbox">
                                                                                        <input type="checkbox" @if($user->hasPermissionTo($permission->name)) checked @endif name="permissions[]" value="{{$permission->name}}" class="custom-control-input" id="{{$permission->id}}{{$u->id}}">
                                                                                        <label class="custom-control-label" for="{{$permission->id}}{{$u->id}}">{{$permission->name}}</label>
                                                                                    </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label for="fname" class="col-sm-4 text-right control-label col-form-label">Status</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="custom-control custom-radio">
                                                                                        <input type="radio" class="custom-control-input" id="status-active-{{$u->id}}" name="status" value="1" <?php if($u->is_active){?> checked <?php } ?>>
                                                                                        <label class="custom-control-label" for="status-active-{{$u->id}}">Active</label>
                                                                                    </div>
                                                                                    <div class="custom-control custom-radio">
                                                                                        <input type="radio" class="custom-control-input" id="status-inactive-{{$u->id}}" name="status" value="0" <?php if(!$u->is_active){?> checked <?php } ?>>
                                                                                        <label class="custom-control-label" for="status-inactive-{{$u->id}}">Inactive</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <input name="id" value="{{$u->id}}" type="hidden">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <form class="form-horizontal" id="editFrm" method="post" action="{{route('register')}}">
                                    @csrf
                                    <input type="hidden" id="editId" name="id">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-4 text-right control-label col-form-label">Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="name" class="form-control" id="nameEdit" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-4 text-right control-label col-form-label">Username</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="username" class="form-control" id="phoneEdit"  required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-4 text-right control-label col-form-label">Password</label>
                                            <div class="col-sm-8">
                                                <input type="password" name="password" class="form-control" id="phoneEdit"  required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-4 text-right control-label col-form-label">Re-type Password</label>
                                            <div class="col-sm-8">
                                                <input type="password" name="password_confirmation" class="form-control" id="phoneEdit"  required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-4 text-right control-label col-form-label">Permissions</label>
                                            <div class="col-sm-8">
                                                @foreach($permissions as $permission)
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="permissions[]" value="{{$permission->name}}" class="custom-control-input" id="{{$permission->id}}add">
                                                        <label class="custom-control-label" for="{{$permission->id}}add">{{$permission->name}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-4 text-right control-label col-form-label">Status</label>
                                            <div class="col-sm-8">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="status-active" name="status" value="1" checked>
                                                    <label class="custom-control-label" for="status-active">Active</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="status-inactive" name="status" value="0">
                                                    <label class="custom-control-label" for="status-inactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="$('#editFrm').submit()">Create User</button>
                </div>
            </div>
        </div>
    </div>
@endsection