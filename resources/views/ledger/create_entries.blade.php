@extends("layouts.app")
@section('title') Upload Ledger Entries @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('create-ledger-entries')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Project</th>
                                    <th>Ledger No.</th>
                                    <th>Date</th>
                                    <th>Bill Amount</th>
                                    <th>Collect Amount</th>
                                    <th width="30%">Particulars</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="project[]" id="" class="form-control project-select" tabindex="1">
                                            <option value="">Select Project</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" class="form-control" name="ledger_no[]"></td>
                                    <td><input type="text" name="date[]" class="select_date form-control"></td>
                                    <td><input type="text" class="form-control" name="bill_amount[]"></td>
                                    <td><input type="text" class="form-control" name="collect_amount[]"></td>
                                    <td><input type="text" class="form-control" name="particulars[]"></td>
                                </tr>
                                </tbody>
                            </table>
                            <input type="submit" value="create ledger entries" class="btn btn-default text-right">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $(".select_date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd"
            });
        })
    </script>
@endsection
