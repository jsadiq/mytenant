@extends("layouts.app")
@section('title') Upload Ledger Entries @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('delete-ledger')}}" method="post">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="text" name="ledger_no" class="form-control" size="150" placeholder="Ledger No." required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <input type="submit" value="delete ledger" onclick="return confirm('Are you sure?');" class="btn btn-default text-right">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
