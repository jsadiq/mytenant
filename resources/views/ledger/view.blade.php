<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
    <style>
        .client th{
            padding-right: 10px;
        }
    </style>
</head>

<body>
    <div class="container">
        <p style="text-align: center;font-weight: bold">
            S.R Trading <br>
            Sher-E Bangla Road, Saidpur, Nilphamari<br>
            {{$project->name}} <br>
            @if($ledger->ledger_type == 'shop')
                Shop Ledger
            @else
                Service Charge Ledger
            @endif
        </p>
        <table class="client">
            <tr>
                <th>Name of Client</th>
                <td>{{$client->name}}</td>
            </tr>
            <tr>
                <th>Floor No.</th>
                <td>{{$shop->floor}}</td>
            </tr>
            <tr>
                <th>Block No.</th>
                <td>{{$shop->block}}</td>
            </tr>
            <tr>
                <th>Shop No.</th>
                <td>{{$shop->shop_no}}</td>
            </tr>
            <tr>
                <th>Mob No.</th>
                <td>{{$client->phone}}</td>
            </tr>
        </table>
        <table class="table table-sm table-bordered">
            <thead>
            <tr>
                <th>Date</th>
                <th>Particular</th>
                <th>Amount<br>Tk</th>
                <th>Received<br>Tk</th>
                <th>Balance<br>Tk</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ledger_entries as $entry)
                <tr>
                    <td nowrap="1">{{date("d M 'y",strtotime($entry->date))}}</td>
                    <td>{{$entry->description}}</td>
                    <td>{{$entry->bill_amount}}</td>
                    <td>{{$entry->received}}</td>
                    <td>{{$entry->balance}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>
