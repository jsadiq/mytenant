@extends("layouts.app")
@section('title')Revenue Report @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('revenue-report')}}">
                            <div class="form-row">
                                <div class="col">
                                    <label for="project">Project:</label>
                                    <select name="project" id="project" class="form-control project-select" tabindex="1" required>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <label for="from">From:</label>
                                    <input type="text" placeholder="from" class="form-control" id="from" name="from" @if(isset($_GET['from'])) value="{{$_GET['from']}}" @endif required>
                                </div>
                                <div class="col">
                                    <label for="to">To:</label>
                                    <input type="text" placeholder="To" class="form-control" id="to" name="to"  @if(isset($_GET['to'])) value="{{$_GET['to']}}" @endif  required>
                                </div>
                                <div class="col">
                                    <label for="to">&nbsp;</label>
                                    <input type="submit" value="Show Report" class="btn btn-default"  style="display: block"/>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($revenues) > 0)
    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-bottom: -27px;">Total: <span id="total" class=""></span> tk. (From {{date("d M 'y", strtotime($_GET['from']))}} to {{date("d M 'y", strtotime($_GET['to']))}})</h4>
        </div>
        <div class="col-md-12">
            <table id="tbl"  class="table table-striped table-bordered compact" style="width:100%">
                <thead>
                <tr>
                    <th>Shop</th>
                    <th>Client</th>
                    <th>Phone</th>
                    <th>Revenue Type</th>
                    <th>Amount</th>
                    <th class="text-right">Collection Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($revenues as $r)
                    <tr>
                        <td>{{$r->shops}}</td>
                        <td>{{$r->name}}</td>
                        <td>{{$r->phone}}</td>
                        <td>{{$r->revenue_type}}</td>
                        <td class="amount text-right" data-amount="{{$r->amount}}">{{number_format($r->amount,2)}}</td>
                        <td class="text-right">{{date("d M 'y",strtotime($r->collection_date))}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script>
        $(".project-select").select2({placeholder: '---select a project---'});
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var dateFormat = "yy-mm-dd",
                from = $("#from").datepicker({
                    changeMonth: true,
                    dateFormat: dateFormat
                }).on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                }),
                to = $("#to").datepicker({
                    changeMonth: true,
                    dateFormat: dateFormat
                }).on("change", function () {
                    from.datepicker("option", "maxDate", getDate(this));
                });
        if($("#from").val() == ''){
            from.datepicker('setDate',firstDay);
        }
        if($("#to").val() == ''){
            to.datepicker('setDate',date);
        }
        $("#tbl").DataTable({
            paging: false
        }).on( 'draw.dt', function () {
            calculate_total();
        });
        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
        calculate_total();
        function calculate_total(){
            var total = 0;
            $(".amount:visible").each(function(){
                total += $(this).data('amount');
            });
            $("#total").text(total.toFixed(2));
        }
    </script>
@endsection