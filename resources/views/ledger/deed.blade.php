@extends("layouts.app")
@section('title')New Deed @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('deed')}}">
                            <div class="form-row">
                                <div class="col">
                                    <select name="project" id="project" class="form-control project-select" tabindex="1" required>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="submit" value="Select Project" class="btn btn-default"  style="display: block"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($_GET['project']))
        <form method="post" action="{{route('save-deed')}}">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="select_date" class="col-sm-3 control-label col-form-label">Date</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control select_date" name="date" id="" value="<?php echo date("Y-m-d"); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-shop" class="col-sm-3 control-label col-form-label">Select Shop</label>
                                <div class="col-sm-6">
                                    <select name="shops[]" id="select-shop" class="form-control shop-select" tabindex="2" required data-bind="event:{change:sel_shop}">
                                        @foreach($shops as $shop)
                                            <option value="{{$shop->id}}" data-price="{{$shop->price}}" data-s_charge="{{$shop->service_charge}}">{{$shop->shop_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-shop" class="col-sm-3 control-label col-form-label">Deed No.</label>
                                <div class="col-sm-6">
                                    <input type="text" name="deed_no" class="form-control" value="<?php echo time() . rand(0,9); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-tenant" class="col-sm-3 control-label col-form-label">Select Client</label>
                                <div class="col-sm-6">
                                    <select name="tenant" id="select-tenant" class="form-control tenant-select" tabindex="2" required>
                                        <option></option>
                                        @foreach($tenants as $tenant)
                                            <option value="{{$tenant->id}}">{{$tenant->name}} - {{$tenant->phone}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-tenant" class="col-sm-3 control-label col-form-label">Advance Service Charge</label>
                                <div class="col-sm-6">
                                    <input type="text" name="adv_service_charge" data-bind="value:adv_s_charge().toFixed(2)" class="form-control"/>
                                    <input type="text" required name="service_charge_note" placeholder="ledger note" class="form-control" style="border: none;border-bottom: 1px solid;" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label>Collect Transfer Fee?</label>
                                </div>
                                <div class="custom-control custom-checkbox col-sm-2" style="margin-left: 10px;">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing1" value="1" name="collect_transfer_fee" data-bind="checked:collect_transfer_fee">
                                    <label class="custom-control-label" for="customControlAutosizing1">yes</label>
                                </div>
                            </div>
                            <div class="form-group row" data-bind="visible: collect_transfer_fee() == true">
                                <label class="col-sm-3 control-label col-form-label">
                                    <label>Transfer Fee ({{$transfer_fee}}%)</label>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text"  data-bind="value:trans_fee()" name="transfer_fee" class="form-control"/>
                                    <input type="text" name="transfer_fee_note" placeholder="ledger note" class="form-control" style="border: none;border-bottom: 1px solid;" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-tenant" class="col-sm-3 control-label col-form-label">Create Ledger Entries</label>
                                <div class="col-sm-2">
                                    <input type="text" name="ledger_entry_from" class="form-control select_date"/>
                                </div>
                                <div class="col-sm-1 text-center">to</div>
                                <div class="col-sm-2">
                                    <input type="text" name="ledger_entry_to" class="form-control select_date"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9 text-right">
                                    <button class="btn btn-default">Create Deed</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="project_id" value="<?php echo $_GET['project']; ?>">
        </form>
    @endif
@endsection

@section('script')
    <script>
        var adv_service_charge = "{{$adv_service_charge}}";
        var transfer_fee = "{{$transfer_fee}}";
        $(".project-select").select2({placeholder: '---select a project---'});
        $(".shop-select").select2({
            multiple:true,
        }).val(null).trigger("change");
        $(".tenant-select").select2({
            placeholder: '---select a client---',
        });

        var viewModel = function(){
            var self = this;
            self.collect_transfer_fee = ko.observable(false);
            self.trans_fee = ko.observable(0);
            self.adv_s_charge = ko.observable(0);
            self.sel_shop = function(){
                var price = 0;
                $("#select-shop option:selected").each(function(){
                    price += $(this).data('price');
                });
                self.trans_fee(price * transfer_fee / 100);

                var s_charge = 0;
                $("#select-shop option:selected").each(function(){
                    s_charge += $(this).data('s_charge');
                });
                self.adv_s_charge(s_charge * adv_service_charge);
            };
        };
        ko.applyBindings(viewModel);

        $(".select_date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd"
        });
    </script>
@endsection