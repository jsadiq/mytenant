@extends("layouts.app")
@section('title')Add Booking @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('booking')}}">
                            <div class="form-row">
                                <div class="col">
                                    <select name="project" id="project" class="form-control project-select" tabindex="1" required>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="submit" value="Select Project" class="btn btn-default"  style="display: block"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($_GET['project']))
        <form method="post" action="{{route('save-booking')}}">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="select_date" class="col-sm-2 control-label col-form-label">Date</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="date" id="select_date" value="<?php echo date("Y-m-d"); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-shop" class="col-sm-2 control-label col-form-label">Select Shop</label>
                                <div class="col-sm-6">
                                    <select name="shops[]" id="select-shop" class="form-control shop-select" tabindex="2" required data-bind="event:{change:set_price}">
                                        @foreach($shops as $shop)
                                            <option value="{{$shop->id}}" data-price="{{$shop->price}}">{{$shop->shop_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-tenant" class="col-sm-2 control-label col-form-label">Select Client</label>
                                <div class="col-sm-6">
                                    <select name="tenant" id="select-tenant" class="form-control tenant-select" tabindex="2" required>
                                        <option></option>
                                        @foreach($tenants as $tenant)
                                            <option value="{{$tenant->id}}">{{$tenant->name}} - {{$tenant->phone}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 control-label col-form-label">Price</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" disabled data-bind="value:price()">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 control-label col-form-label">Down Payment</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="down_payment" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-2 control-label col-form-label">Note</label>
                                <div class="col-sm-6">
                                    <textarea name="note" class="form-control" required placeholder="note here"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-8 text-right">
                                    <button class="btn btn-default">Add Booking</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="project_id" value="<?php echo $_GET['project']; ?>">
        </form>
    @endif
@endsection

@section('script')
    <script>
        $(".project-select").select2({placeholder: '---select a project---'});
        $(".shop-select").select2({
            multiple:true,
        }).val(null).trigger("change");
        $(".tenant-select").select2({
            placeholder: '---select a client---',
        });

        var viewModel = function(){
            var self = this;
            self.price = ko.observable(null);
            self.set_price = function(){
                var price = 0;
                $("#select-shop option:selected").each(function(){
                    price += $(this).data('price');
                });
                self.price(price);
            }

        };
        ko.applyBindings(viewModel);

        $("#select_date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "yy-mm-dd"
        });
    </script>
@endsection