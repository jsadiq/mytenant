@extends("layouts.app")
@section('title')Ledgers ({{$ledger_type}}) @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('ledgers',['ledger_type'=>$ledger_type])}}">
                            <div class="form-row">
                                <div class="col">
                                    <select name="project" id="project" class="form-control project-select" tabindex="1" required>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="submit" value="Select Project" class="btn btn-default"  style="display: block"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="custom-control custom-checkbox col-sm-12" style="margin-left: 5px;margin-top: 8px;">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing1" value="1" name="show_cancelled_ledgers" @if(isset($_GET['show_cancelled_ledgers'])) checked @endif>
                                    <label class="custom-control-label" for="customControlAutosizing1" style="margin-left: 15px;">show cancelled ledgers</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="tbl"  class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>Ledger No.</th>
                    <th>Shop</th>
                    <th>Client</th>
                    @if($ledger_type == 'shop')
                    <th>Price</th>
                    <th>Collect</th>
                    @endif
                    <th>Balance</th>
                    <th>Deed No.</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ledgers as $ledger)
                    <tr>
                        <td><a href="#" class="ledger_link" data-url="{{route('ledger',['ledger_id' => $ledger->id])}}">{{$ledger->ledger_no}}</a></td>
                        <td><small><?php echo $ledger->shops; ?></small></td>
                        <td>{{$ledger->client}}</td>
                        @if($ledger_type == 'shop')
                        <td>
                            @if($ledger->sales_price)
                                {{$ledger->sales_price}}
                            @endif
                        </td>
                        <td>
                            @if($ledger->sales_price)
                                {{$ledger->sales_price - $ledger->balance}}
                            @endif
                        </td>
                        @endif
                        <td>{{$ledger->balance}}</td>
                        <td>{{$ledger->deed_no}}</td>
                        <td>
                            @if($ledger->is_active == 1)
                            <span style="color: green">active</span>
                            @else
                            <span style="color: gray">cancelled</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(".project-select").select2({placeholder: '---select a project---'});
        $(".shop-select").select2({
            multiple:true,
        }).val(null).trigger("change");
        $("#tbl").DataTable({
            paging: false
        });
        $(document).ready(function(){
            $(".ledger_link").click(function(e){
                e.preventDefault();
                window.open($(this).data('url'), '_blank', 'left=300,top=100,height=570,width=520,toolbar=no,location=no,scrollbars=yes,status=yes');
            });
        })
    </script>
@endsection