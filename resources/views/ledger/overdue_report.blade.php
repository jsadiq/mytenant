@extends("layouts.app")
@section('title')Revenue Report @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('overdue-report')}}">
                            <div class="form-row">
                                <div class="col">
                                    <select name="project" id="project" class="form-control project-select" tabindex="1" required>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="submit" value="Select Project" class="btn btn-default"  style="display: block"/>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($overdue) > 0)
    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-bottom: -27px;">Total Overdue: <span id="total" class=""></span> tk.</h4>
        </div>
        <div class="col-md-12">
            <table id="tbl"  class="table table-striped table-bordered compact" style="width:100%">
                <thead>
                <tr>
                    <th>Shop</th>
                    <th>Client</th>
                    <th>Phone</th>
                    <th>Overdue Type</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach($overdue as $r)
                    <tr>
                        <td>{{$r->shops}}</td>
                        <td>{{$r->name}}</td>
                        <td>{{$r->phone}}</td>
                        <td>@if($r->ledger_type == 'shop') Shop Price @else Service Charge @endif</td>
                        <td class="amount text-right" data-amount="{{$r->balance}}">{{number_format($r->balance,2)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script>
        $(".project-select").select2({placeholder: '---select a project---'});
        $("#tbl").DataTable({
            paging: false
        }).on( 'draw.dt', function () {
            calculate_total();
        });
        calculate_total();
        function calculate_total(){
            var total = 0;
            $(".amount:visible").each(function(){
                total += $(this).data('amount');
            });
            $("#total").text(total.toFixed(2));
        }
    </script>
@endsection