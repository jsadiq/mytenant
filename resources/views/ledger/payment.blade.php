@extends("layouts.app")
@section('title') Collect Payment @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('payment')}}">
                            <div class="form-row">
                                <div class="col">
                                    <select name="client" id="client" class="form-control project-select" tabindex="1" required>
                                        <option></option>
                                        @foreach($clients as $client)
                                            <option value="{{$client->id}}" @if(isset($_GET['client']) && $_GET['client'] == $client->id) selected @endif>{{$client->name}} ({{$client->phone}})</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="submit" value="Select Client" class="btn btn-default"  style="display: block"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Project</th>
                                <th>Shop</th>
                                <th>Ledger No</th>
                                <th>Payment Type</th>
                                <th>Due</th>
                                <th>Collect Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dues as $d)
                                <tr>
                                    <td>{{$d->name}}</td>
                                    <td>{{$d->shops}}</td>
                                    <td>{{$d->ledger_no}}</td>
                                    <td>
                                        @if($d->ledger_type == 'shop')
                                            sales price
                                        @else
                                            service charge
                                        @endif
                                    </td>
                                    <td>{{$d->balance}}</td>
                                    <td>
                                        <form action="{{route('payment')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="project_id" value="{{$d->project_id}}">
                                            <input type="hidden" name="client" value="{{$_GET['client']}}">
                                            <input type="hidden" name="ledger_type" value="{{$d->ledger_type}}">
                                            <input type="hidden" name="ledger_id" value="{{$d->ledger_id}}">
                                            <input type="number" name="amount" required style="width: 90px;">
                                            <input type="text" name="note" placeholder="ledger note" required>
                                            <button class="btn btn-sm btn-default">collect</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(".project-select").select2({placeholder: '---select a client---'});
    </script>
@endsection