@extends("layouts.app")
@section('title')Shops @endsection
@section('content')
    @if(isset($_GET['project']))
        <div class="row">
            <div class="col-md-12 text-right">
                <p>
                    <button data-toggle="modal" data-target="#add"  class="btn btn-primary">Create a shop</button>
                </p>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form action="{{route('shop-list')}}">
                            <div class="form-row">
                                <div class="col">
                                    <select name="project" id="project" class="form-control project-select" tabindex="1" required>
                                        @foreach($projects as $project)
                                            <option value="{{$project->id}}" @if(isset($_GET['project']) && $_GET['project'] == $project->id) selected @endif>{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="submit" value="Select Project" class="btn btn-default"  style="display: block"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(count($shops) > 1)
        @role('manager')
        <div class="row">
            <div class="col-md-12 text-left" style="">
                <p>
                    <button id="update_amount_btn" class="btn btn-sm btn-default">update selected shops' price/service charge</button>
                </p>
            </div>
        </div>
        @endrole
        <div class="row">
            <div class="col-md-12">
                <table id="tbl"  class="table table-striped table-bordered compact" style="width:100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox" class="sel-all"></th>
                        <th>Code</th>
                        <th>Shop No.</th>
                        <th>Block</th>
                        <th>Floor</th>
                        <th>Size</th>
                        <th>Client</th>
                        <th>Phone</th>
                        <th>Price</th>
                        <th>S.Charge</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($shops as $shop)
                            <tr>
                                <td><input type="checkbox" class="sel-record" value="{{$shop->id}}"></td>
                                <td>{{$shop->shop_code}}</td>
                                <td>{{$shop->shop_no}}</td>
                                <td>{{$shop->block}}</td>
                                <td>{{$shop->floor}}</td>
                                <td>{{$shop->size}} sqft</td>
                                <td>{{$shop->tenant}}</td>
                                <td>{{$shop->tenant_phone}}</td>
                                <td>{{number_format($shop->price, 2)}}</td>
                                <td>{{number_format($shop->service_charge, 2)}}</td>
                                <td>
                                    <a
                                            data-id = "{{$shop->id}}"
                                            data-shop-no = "{{$shop->shop_no}}"
                                            data-floor = "{{$shop->floor_id}}#{{$shop->block_id}}"
                                            data-size = "{{$shop->size}}"
                                            data-type = "{{$shop->type}}"
                                            data-price = "{{$shop->price}}"
                                            data-service-charge = "{{$shop->service_charge}}"
                                            data-project = "{{$shop->project_id}}"
                                            class="edit-shop" href=""><i class="mdi mdi-square-edit-outline"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
    @if(isset($_GET['project']))
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Shop Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <form class="form-horizontal" id="addFrm" method="post" action="{{route('shop-save')}}">
                                    @csrf
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Shop no. *</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="number" tabindex="1" class="form-control" id="" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname"class="col-sm-3 text-right control-label col-form-label">Floor/Block *</label>
                                            <div class="col-sm-9">
                                                <select name="floor" id="floor" tabindex="2" class="form-control custom-select-tags" required style="width: 100%">
                                                    @foreach($floors as $f)
                                                        <option value="{{$f->id}}">{{$f->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Price *</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="amount" tabindex="1" class="form-control" id="" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Service Charge *</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="service_charge" tabindex="1" class="form-control" id="" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Size (sqft) *</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="size" tabindex="1" class="form-control" id="" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-9">
                                                <small id="" class="form-text text-muted">
                                                    *  required fields
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="project" value="{{$_GET['project']}}">
                                    <input type="hidden" name="id" value="">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="$('#addFrm').submit()">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update_amount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update price/rent/service charge</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <form class="form-horizontal" id="amtFrm" method="post" action="{{route('shop-update-amount')}}">
                                    @csrf
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-7 text-right control-label col-form-label">update service charge / rent by:</label>
                                            <div class="col-sm-5">
                                                <input type="text" name="service_charge" tabindex="1" class="form-control" id="" style="margin-bottom: 6px">
                                                <input type="radio" id="rt54" name="service_charge_update_type" value="percent" checked> <label for="rt54">percent</label>
                                                <input type="radio" id="gtig" name="service_charge_update_type" value="taka"> <label for="gtig">taka</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname"class="col-sm-7 text-right control-label col-form-label">Update price by:</label>
                                            <div class="col-sm-5">
                                                <input type="text" name="price" tabindex="1" class="form-control" id="" style="margin-bottom: 6px">
                                                <input type="radio" id="jdu7" name="price_update_type" value="percent" checked> <label for="jdu7">percent</label>
                                                <input type="radio" id="kd86" name="price_update_type" value="taka"> <label for="kd86">taka</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="$('#amtFrm').submit()">Submit</button>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script>
        $.fn.modal.Constructor.prototype._enforceFocus = function() {};
        $(".project-select").select2({placeholder: '---select a project---'});
        $(".tenant-select").select2({placeholder: '---no tenant---'});
        $(".custom-select-tags").select2({width: 'resolve'});
        $("#tbl").DataTable({
            paging: false,
            order: [[ 1, 'asc' ]],
            searching: true,
            columnDefs: [ {
                "targets": [0, 10],
                "sortable": false,
            },{
                "targets": [0,4,5,8,9,10],
                "searchable": false,
            } ],
        });
        $('#add').on('hide.bs.modal', function (e) {
            var project_id = $("#addFrm").find("[name=project]").val();
            $("#addFrm")[0].reset();
            $("#addFrm").find("[name=project]").val(project_id);
            $("#addFrm").find("[name=tenant]").val(null).trigger("change")
        });
        $(".edit-shop").click(function(e){

            e.preventDefault();

            var frm = $("#addFrm");
            var info = $(this).data();
            frm.find("[name=number]").val(info.shopNo);
            frm.find("[name=floor]").val(info.floor).trigger("change");
            frm.find("[name=block]").val(info.block).trigger("change");
            frm.find("[name=amount]").val(info.price);
            frm.find("[name=service_charge]").val(info.serviceCharge);
            frm.find("[name=size]").val(info.size);
            frm.find("[name=id]").val(info.id);
            $("#add").modal();
        });
        $(".sel-all").click(function(){
            $(".sel-record").prop("checked",$(this).is(":checked"));
        });
        $('#update_amount_btn').on('click', function (e) {
            if($(".sel-record:checked").length == 0){
                alert("No shop selected");
            }else{
                $("#amtFrm")[0].reset();
                var id_arr = [];
                $(".sel-record:checked").each(function(){
                   id_arr.push($(this).val());
                });
                $("#amtFrm input[name=id]").val(id_arr.join(','));
                $("#update_amount").modal();
            }
        });
    </script>
@endsection