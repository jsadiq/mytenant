@extends("layouts.app")
@section('title') Settings @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <form method="POST" action="{{route('settings')}}" style="width: 100%">
                            @csrf
                            @foreach($settings as $setting)
                                <div class="form-group row">
                                    <label for="{{$setting->settings_name}}" class="col-sm-3 text-right control-label col-form-label">{{$setting->settings_label}}:</label>
                                    <div class="col-sm-2">
                                        <input type="number" min="0" class="form-control" name="{{$setting->settings_name}}" id="{{$setting->settings_name}}" value="{{$setting->value}}">
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-2 text-right">
                                    <input type="submit" class="btn btn-primary" value="SAVE">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
