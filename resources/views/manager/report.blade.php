@extends("layouts.app")
@section('title')Revenue Report @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <select name="project" id="project" class="form-control select project-select custom-select" tabindex="1" required>
                                <option value="" selected>select project</option>
                                @foreach($projects as $project)
                                    <option value="{{$project->id}}">{{$project->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-default"  style="display: block" onclick="selectProject()">Select Project</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="filtering-options" style="display: none">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="from">From:</label>
                            <input type="text" placeholder="from" class="form-control" id="from" name="from" required>
                        </div>
                        <div class="col-md-2">
                            <label for="to">To:</label>
                            <input type="text" placeholder="To" class="form-control" id="to" name="to" required>
                        </div>
                        <div class="col-md-3">
                            <label for="" style="display: block" for="tenant">Tenant:</label>
                            <select name="tenant" class="form-control select2 custom-select" id="tenant">
                                <option value="">All</option>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <label for="" style="display: block" for="shop">Shop no.:</label>
                            <select name="shop" class="form-control select2 custom-select" id="shop">
                                <option value="">All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="" style="display: block" for="rev_type">Revenue type:</label>
                            <select name="rev_type" class="form-control select2 custom-select" id="rev_type">
                                <option value="">All</option>
                                <option value="service charge">Service Charge</option>
                                <option value="down payment">Down Payment</option>
                                <option value="installment">Installment</option>
                                <option value="transfer fee">Transfer Fee</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="">&nbsp;</label>
                            <button class="btn btn-secondary"  style="display: block" data-bind="click:generateReport">Generate Report</button>
                        </div>
                        <div class="col-md-6" style="margin-top: 10px">
                            <input type="checkbox" name="overdue_only" value="1">&nbsp;&nbsp;Show only overdue payments
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" data-bind = "visible: result().length > 0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 text-right" style="margin-bottom: 12px">
                            <button  data-toggle="modal" data-target="#sms_form"  id="send_sms" class="btn btn-sm btn-primary" style="visibility: hidden;">Send SMS to selected tenants</button>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                          <table class="table table-sm">
                              <thead>
                                <tr>
                                    <th><input type="checkbox" class="sel-all"></th>
                                    <th>Date</th>
                                    <th>Due Date</th>
                                    <th>Shop No.</th>
                                    <th>Tenant</th>
                                    <th>Phone</th>
                                    <th>Revenue Type</th>
                                    <th>Amount</th>
                                </tr>
                              </thead>
                              <tbody data-bind="foreach: result">
                                <tr>
                                    <td><input type="checkbox" class="sel-record" data-bind="value:id, attr:{'data-tenant-name':tenant_name, 'data-tenant-phone':tenant_phone}"></td>
                                    <td data-bind="text: formattedDate(collection_date), css: {'table-danger': collection_date == null}"></td>
                                    <td data-bind="text:  formattedDate(due_date)"></td>
                                    <td data-bind="text: shop_no"></td>
                                    <td data-bind="text: tenant_name"></td>
                                    <td data-bind="text: tenant_phone"></td>
                                    <td data-bind="text: revenue_type.toUpperCase()"></td>
                                    <td data-bind="text: amount.toFixed(2)"></td>
                                </tr>
                              </tbody>
                          </table>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="links"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--the spinner--}}
    <div class="lds-css ng-scope" style="display: none">
        <div class="lds-blocks" style="100%;height:100%;margin: auto"><div style="left:38px;top:38px;animation-delay:0s"></div><div style="left:80px;top:38px;animation-delay:0.125s"></div><div style="left:122px;top:38px;animation-delay:0.25s"></div><div style="left:38px;top:80px;animation-delay:0.875s"></div><div style="left:122px;top:80px;animation-delay:0.375s"></div><div style="left:38px;top:122px;animation-delay:0.75s"></div><div style="left:80px;top:122px;animation-delay:0.625s"></div><div style="left:122px;top:122px;animation-delay:0.5s"></div></div>
        <style type="text/css">@keyframes lds-blocks {
                                   0% {
                                       background: #45aee7;
                                   }
                                   12.5% {
                                       background: #45aee7;
                                   }
                                   12.625% {
                                       background: #0055a5;
                                   }
                                   100% {
                                       background: #0055a5;
                                   }
                               }
            @-webkit-keyframes lds-blocks {
                0% {
                    background: #45aee7;
                }
                12.5% {
                    background: #45aee7;
                }
                12.625% {
                    background: #0055a5;
                }
                100% {
                    background: #0055a5;
                }
            }
            .lds-blocks {
                position: relative;
            }
            .lds-blocks div {
                position: absolute;
                width: 40px;
                height: 40px;
                background: #0055a5;
                -webkit-animation: lds-blocks 1s linear infinite;
                animation: lds-blocks 1s linear infinite;
            }
            .lds-blocks {
                width: 115px !important;
                height: 115px !important;
                -webkit-transform: translate(-57.5px, -57.5px) scale(0.575) translate(57.5px, 57.5px);
                transform: translate(-57.5px, -57.5px) scale(0.575) translate(57.5px, 57.5px);
            }
        </style></div>


    @component("sms")
    @endcomponent
@endsection

@section('script')
    <script>
        $(".select2").select2({width: "100%"});
        $(".project-select").select2({placeholder: '---select a project---'});
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var dateFormat = "yy-mm-dd",
                from = $("#from").datepicker({
                            changeMonth: true,
                            dateFormat: dateFormat
                        })
                        .on("change", function () {
                            to.datepicker("option", "minDate", getDate(this));
                        }),
                to = $("#to").datepicker({
                    changeMonth: true,
                    dateFormat: dateFormat
                }).on("change", function () {
                            from.datepicker("option", "maxDate", getDate(this));
                        });
        from.datepicker('setDate',firstDay);
        to.datepicker('setDate',date);
        var selected_project = '';
        var from_date = '';
        var to_date = '';
        var tenant = '';
        var shop = '';
        var rev_type = '';
        var overdue_only = '';
        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
        var selectProject = function () {
            if ($("#project").val() == '') {
                alert("select a project");
                return;
            }
            selected_project = $("#project").val();
            $.ajax(app_path + '/api/getFilteringOption', {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                data: {
                    project_id: $("#project").val(),
                },
                success: function (data) {
                    $("#filtering-options").show();
                    $("select[name=tenant]").empty().append("<option value=''>All</option>");
                    $("select[name=shop]").empty().append("<option value=''>All</option>");
                    data.tenants.forEach(function(tenant){
                        $("select[name=tenant]").append("<option value='" + tenant.id + "'>" + tenant.name + " (" + tenant.phone + ") </option>");
                    });
                    data.shops.forEach(function(shop){
                        $("select[name=shop]").append("<option value='" + shop.id + "'>" + shop.shop_no + "</option>");
                    });
                }
            })
        };
        var result = ko.observableArray([]);
        var generateReport = function(event){
            result([]);
            if($("#from").val() == ''){
                alert("select a from date");
                return;
            }
            if($("#to").val() == ''){
                alert("select a to date");
                return;
            }
            from_date = $("#from").val();
            to_date = $("#to").val();
            tenant = $("#tenant").val();
            shop = $("#shop").val();
            rev_type = $("#rev_type").val();
            overdue_only = $("input[name=overdue_only]").is(":checked");
            getData(1);
        };
        var getData = function(page){
            $(".lds-css").show();
            $.ajax(app_path + "/management/generate-report",{
                method: "get",
                data: {
                    project: selected_project,
                    from: from_date,
                    to: to_date,
                    tenant: tenant,
                    shop: shop,
                    revenue_type: rev_type,
                    only_overdue: overdue_only,
                    page: page
                },
                dataType: "json",
                success: function(res){
                    self.result(res.data);
                    var links = $('<textarea />').html(res.links).text();
                    $("#links").html(links);
                    $(".lds-css").hide();
                    if(res.data.length == 0){
                        alert("no data found.");
                    }
                }
            });
        };
        var formattedDate = function(date){
            if(date == null){
                return '-';
            }
            return moment(date).format("DD MMM, YYYY");
        };

        ko.applyBindings(result);
        $("#links").on('click',".page-link",function(e){
            e.preventDefault();
            var url = new URL($(this).prop("href"));
            var page = url.searchParams.get("page");
            getData(page);
        });
        $(".sel-all").click(function(){
            $(".sel-record").prop("checked",$(this).is(":checked"));
            if($(this).is(":checked")){
                $("#send_sms").css('visibility','visible');
            }else{
                $("#send_sms").css('visibility','hidden');
            }
        });
        $("body").on('click','.sel-record',function(){
            if($(".sel-record:checked").length > 0){
                $("#send_sms").css('visibility','visible');
            }else{
                $("#send_sms").css('visibility','hidden');
            }
        });
    </script>
@endsection