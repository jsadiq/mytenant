@extends('layouts.app')
@section('content')
@if(isset($_GET['invoice_no']))
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="height: 900px">
                            <div class="card-body">
                                <iframe style="border: none; width: 100%; height: 100%;" src="{{route('invoice',['invoice_id'=>$_GET['invoice_no']])}}"></iframe>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@else
    <?php $bg = ['bg-danger', 'bg-info', 'bg-success', 'bg-cyan', 'bg-warning']; shuffle($bg); ?>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="{{array_pop($bg)}} p-10 text-white text-center box">
                    <i class="fa fa-folder-open m-b-5 font-16"></i>
                    <h5 class="" style="font-size: 36px">{{$no_of_projects}}</h5>
                    <h6 class="text-white">Projects</h6>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="{{array_pop($bg)}} p-10 text-white text-center box">
                    <i class="fa fa-home m-b-5 font-16"></i>
                    <h5 class="" style="font-size: 36px">{{$shops}}</h5>
                    <h6 class="text-white">Shops</h6>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="{{array_pop($bg)}} p-10 text-white text-center box">
                    <i class="fa fa-user m-b-5 font-16"></i>
                    <h5 class="" style="font-size: 36px">{{$clients}}</h5>
                    <h6 class="text-white">Clients</h6>
                </div>
            </div>
        </div>
    </div>
    @foreach($projects_info as $p):
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center">
                        <div>
                            <h4 class="card-title">{{$p['project_name']}}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-2">
                            <div class="row">
                                <div class="col-md-12 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center box">
                                        <i class="fa fa-home m-b-5 font-16"></i>
                                        <h5 class="">{{$p['shops']}}</h5>
                                        <small class="font-light">Total Shops</small>
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-lock m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{$p['vacant_shops']}}</h5>
                                        <small class="font-light">Vacant Shops</small>
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-circle m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{number_format($p['overdue'],2)}}</h5>
                                        <small class="font-light">Total Overdue</small>
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-money-bill-alt m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{number_format($p['collection_this_month'],2)}}</h5>
                                        <small class="font-light">Revenue this month</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <div id="chart-{{$p['project_id']}}" style="height: 100%; width: 100%"></div>
                            <input type="hidden" class="rev_data" data-proj-id="{{$p['project_id']}}" value="{{$p['rev_data']}}">
                        </div>
                        <!-- column -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endif
@endsection

@section('script')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
        $("document").ready(function(){
            $(".rev_data").each(function(){
               var id = $(this).data('proj-id');
               var data = JSON.parse($(this).val());
                if(data.length > 0 ){
                    draw_chart('chart-' + id, data);
                }
            });
        });
        function draw_chart(id, data){
            var chart = new CanvasJS.Chart(id, {
                animationEnabled: true,
                theme: "light2", // "light1", "light2", "dark1", "dark2"
                title:{
                    text: "Monthly Revenue",
                    fontSize: 20
                },
                axisY: {
                    title: "Revenue"
                },
                data: [{
                    type: "column",
                    dataPoints: data
                }],
                axisX: {
                    interval: 1,
                }
            });
            chart.render();
        }
    </script>
@endsection
