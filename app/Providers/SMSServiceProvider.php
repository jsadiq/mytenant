<?php

namespace App\Providers;

use App\MyTenantSMS\GreenWeb\GreenWeb;
use Illuminate\Support\ServiceProvider;

class SMSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\MyTenantSMS\SMS',function(){
            return new GreenWeb();
        });
    }
}
