<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class UpdateRevenuesTable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $last_update = Storage::get("last_update.txt");
        $today = date("Y-m-d");
        if($last_update == $today){
            return true;
        }

        if(empty($last_update)){
            $dt = $today;
        }else{
            $dt = date("Y-m-d", strtotime($last_update . "+1 days"));
        }
        while($dt <= $today){
            /*is it the first day of month*/
            $parts = explode('-',$dt);
            if($parts[2] == '01'){
                /*adding ledger entries*/
                $ledgers = DB::table('ledgers')
                    ->join('ledger_shops','ledgers.id','=','ledger_shops.ledger_id')
                    ->join('shops','shops.id','=','ledger_shops.shop_id')
                    ->where([
                    'ledger_type' => 'service_charge',
                    'is_active' => 1
                ])->get();
                foreach($ledgers as $ledger){
                    DB::transaction(function () use ($ledger, $dt) {
                        $current_balance = DB::table('ledger_balance')->select('balance')
                                ->where('ledger_id',$ledger->ledger_id)
                                ->first()->balance ?? 0;
                        DB::table('ledger_entries')->insert([
                            'ledger_id' => $ledger->ledger_id,
                            'date' => $dt,
                            'description' => "service charge - " . date("M 'y", strtotime($dt)),
                            'bill_amount' => $ledger->service_charge,
                            'balance' => $current_balance + $ledger->service_charge
                        ]);
                        DB::table('ledger_balance')->where('ledger_id','=',$ledger->ledger_id)->update(['balance' => $current_balance + $ledger->service_charge]);

                    });
                }

            }
            $dt = date("Y-m-d", strtotime($dt . "+1 days"));
        }

        Storage::put("last_update.txt", $today);
    }
}
