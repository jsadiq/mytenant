<?php
/**
 * Created by PhpStorm.
 * User: jafor
 * Date: 2019-01-22
 * Time: 10:03 PM
 */

namespace App\MyTenantSMS\GreenWeb;

use App\MyTenantSMS\SMS;

class GreenWeb implements SMS{
    public function send($numbers = [],$message){

        $to = implode(",",$numbers);
        $token = "8b5b8c1cea62e1608534d6cdb576ffc8";

        $url = "http://api.greenweb.com.bd/api.php";


        $data= array(
            'to'=>"$to",
            'message'=>"$message",
            'token'=>"$token"
        ); // Add parameters in key value
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        $smsresult = curl_exec($ch);
        if(strpos($smsresult, "Error:") === 0){
            return ['status' => 'error', 'msg' => $smsresult];
        }
        return ['status' => 'success', 'msg' => $smsresult];
    }
}