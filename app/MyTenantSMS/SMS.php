<?php
namespace App\MyTenantSMS;

interface SMS{
    public function send($numbers = [],$message);
}