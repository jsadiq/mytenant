<?php

namespace App\Http\Controllers;

use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use PHPUnit\Framework\Error\Error;

class TenantController extends Controller
{
    private  $projects = [];
    public function __construct(){
        $this->projects = DB::table('projects')->get();
    }
    public function tenantList(Request $req){
        $sql_shops = "SELECT id tenant_id, GROUP_CONCAT(shops) shops
FROM (SELECT tenants.name tenant, tenants.id id, CONCAT(GROUP_CONCAT(shop_no),' block-',blocks.name,' ', floors.name) shops
FROM ledgers JOIN tenants ON ledgers.tenant_id = tenants.id JOIN ledger_shops ON ledger_shops.ledger_id = ledgers.id JOIN shops ON shops.id = ledger_shops.shop_id JOIN blocks ON blocks.id = shops.block_id JOIN floors ON floors.id = shops.floor_id
WHERE ledgers.ledger_type = 'shop'
GROUP BY tenants.id, blocks.id) AS tbl
GROUP BY tbl.id";

        $sql_dues = "SELECT ledgers.tenant_id, SUM(balance) due FROM ledgers JOIN ledger_balance ON ledgers.id = ledger_balance.ledger_id
WHERE ledgers.is_active = 1
GROUP BY tenant_id";

        $tenants = DB::table('tenants')
            ->leftJoinSub($sql_shops, 'shops', 'shops.tenant_id','=','tenants.id')
            ->leftJoinSub($sql_dues, 'overdues', 'overdues.tenant_id','=','tenants.id')
            ->get();
        return view('tenant/list',['projects'=>$this->projects, 'tenants' => $tenants]);
    }
    public function save(Request $req){
        if(empty($req->input('name')) || empty($req->input('phone'))){
            return redirect(route('tenant-list'))->with('error', 'Name and phone number are required.');
        }
        $data = [
            'name' => $req->input('name'),
            'phone' => $req->input('phone'),
            'title' => $req->input('title'),
        ];

        try{
            if($req->input('id')){
                DB::table('tenants')->where('id','=',$req->input('id'))->update($data);
                $msg = "Record updated.";
            }else{
                DB::table('tenants')->insert($data);
                $msg = "New tenant created.";
            }
        }catch (\Illuminate\Database\QueryException $e){
            return redirect(route('tenant-list'))->with('error', 'Error: '.$e->getMessage());
        }
        return redirect(route('tenant-list'))->with('success', $msg);

    }
}
