<?php

namespace App\Http\Controllers;

use App\MyTenantSMS\SMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ManagementController extends Controller
{
    private  $projects = [];
    public function __construct(){
        $this->projects = DB::table('projects')->get();
    }
    public function showReport(){
        return view('manager.report',['projects' => $this->projects]);
    }
    public function generateReport(Request $req){
        if(!$req->get('project') || !$req->get('from') || !$req->get('to')){
            return "too few params.";
        }
        $report = DB::table('revenues')
            ->where('project_id', '=', $req->get('project'))
            ->where(function($q) use ($req){
               $q->whereBetween('collection_date',[$req->get('from'), $req->get('to')])
                   ->orWhere(function($q) use ($req){
                       $q->whereBetween('due_date',[$req->get('from'), $req->get('to')])
                           ->whereNull('collection_date');
                   });
            });
        if($req->get('tenant')){
            $report->where('tenant_id','=',$req->get('tenant'));
        }
        if($req->get('shop')){
            $report->where('shop_id','=',$req->get('shop'));
        }
        if($req->get('revenue_type')){
            $report->where('revenue_type','=',$req->get('revenue_type'));
        }
        if($req->get('only_overdue') == 'true'){
            $report->whereNull('collection_date');
        }
        $res = $report->paginate(500);
        return json_encode([
            'data' => $res->items(),
            'links' => htmlspecialchars($res->links())
        ]);
    }

    public function settings(Request $req){
        if($req->method() == 'GET'){
            $settings = DB::table('settings')->get();
            return View::make('manager.settings',['settings' => $settings]);
        }
        if($req->method() == 'POST'){
            foreach($req->all() as $setting => $val){
                if($setting != '_token'){
                    DB::table('settings')
                        ->where('settings_name', $setting)
                        ->update(['value' => $val]);
                }
            }
            return redirect()->route('settings');
        }
    }

    public function send_sms(Request $req, SMS $sms){
        $to = $req->input('to');
        //$to = [];
        if($req->input('others')){
            $others = array_map(function($val){
                return trim($val);
            },explode(',',$req->input('others')));
            $to = array_merge($to, $others);
        }
        if($req->input('common_sms') && $req->input('common_sms') == 'yes'){
            $resp = $sms->send($to,$req->input('message'));
        }else{
            $tokens = ['client_title','client_name','shop_price_due','service_charge_due','total_due'];
            foreach($to as $phone){
                $token_values = $this->get_token_values($phone);
                if(!$token_values || count($token_values) != count($tokens)) continue;
                $message = str_replace($tokens,$token_values,$req->input('message'));
                $resp = $sms->send($phone,$message);
            }
        }

        return $resp;

    }

    private function get_token_values($phone){
        /* client_title, client_name */
        $client = DB::table('tenants')->where('phone','=',$phone)->first();
        if(!$client) return false;
        $client_title = $client->title;
        $client_name = $client->name;
        $shop_price_res = DB::table('ledgers')->join('ledger_balance','ledgers.id','=','ledger_balance.ledger_id')
            ->where([
                ['ledger_type','=','shop'],
                ['is_active','=',1],
                ['balance','>',0],
                ['ledgers.tenant_id','=',$client->id],
            ])->first();
        $shop_price_due = $shop_price_res ? $shop_price_res->balance : 0;

        $scharge_price_res = DB::table('ledgers')->join('ledger_balance','ledgers.id','=','ledger_balance.ledger_id')
            ->where([
                ['ledger_type','=','service_charge'],
                ['is_active','=',1],
                ['balance','>',0],
                ['ledgers.tenant_id','=',$client->id],
            ])->first();
        $service_charge_due = $scharge_price_res ? $scharge_price_res->balance : 0;

        $total_due = $shop_price_due + $service_charge_due;

        return [$client_title, $client_name, $shop_price_due, $service_charge_due, $total_due];
    }
}
