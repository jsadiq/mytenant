<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }

    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required|string|max:40|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'name'     => 'required|max:100',
        ]);
        $user = User::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'is_active' => $request->input('status') == "1"
        ]);
        foreach($request->input('permissions') as $permission){
            $user->givePermissionTo($permission);
        }
        if($user){
            return redirect("/")->with('success','User ' . $request->input('name'). ' created.');
        }
    }

    public function editUser(Request $request){
        $validation = [
            'username' => [
                'required','string','max:40',
                Rule::unique('users')->ignore($request->input('id'))
            ],
            'name'     => 'required|max:100',
        ];
        if($request->input('password')){
            $validation['password'] = 'string|min:6|confirmed';
        }
        $data = [
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'is_active' => $request->input('status') == "1"
        ];
        if($request->input('password')){
            $data['password'] = Hash::make($request->input('password'));
        }
        $request->validate($validation);
        User::where('id',$request->input('id'))->update($data);
        $user = User::where('id',$request->input('id'))->first();
        /*removing all permissions*/
        $permissions = Permission::all();
        foreach($permissions as $permission){
            $user->revokePermissionTo($permission->name);
        }

        foreach($request->input('permissions') as $permission){
            $user->givePermissionTo($permission);
        }

        return redirect("/")->with('success','User record update.');
    }

    public function updatePassword(Request $req){
        $pw = Auth::user()->getAuthPassword();
        if($req->isMethod('get')){
            return view('reset_password');
        }else{
            $validator = \Illuminate\Support\Facades\Validator::make($req->all(), [
                'new_password' => 'required|min:6|confirmed',
                'password' => 'required',
            ]);
            $validator->after(function ($validator) use ($req, $pw) {
                if (Hash::check($req->input('password'),$pw) == false) {
                    $validator->errors()->add('password', 'Password is not correct.');
                }
            });
            $validator->validate();
            User::where('id',Auth::id())->update(['password' => Hash::make($req->input('new_password'))]);
            return view('reset_password',['success_msg' => 1, 'success'=>'Password updated.']);
        }
    }
}
