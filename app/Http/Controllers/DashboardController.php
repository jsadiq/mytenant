<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(Request $req){

        $user = \Illuminate\Support\Facades\Auth::user();

        if($user->hasRole('super_admin')){
            $users = DB::table('users')->where('id','<>',1)->get();
            return view('users.list',compact("users"));
        }

        $no_of_projects = DB::table('projects')->count();
        $clients = DB::table('tenants')->count();
        $shops = DB::table('shops')->count();
        $projects_info = [];

        $projects = DB::table('projects')->get();

        foreach($projects as $p){
            $no_of_shops  = DB::table('shops')->where('project_id','=',$p->id)->count();
            $no_of_occupied_shops = DB::table('ledgers')
                ->join('ledger_shops','ledgers.id','=','ledger_shops.ledger_id')
                ->join('shops','shops.id','=','ledger_shops.shop_id')
                ->where(['ledgers.is_active' => 1, 'shops.project_id' => $p->id])->count();
            $vacant_shops = $no_of_shops - $no_of_occupied_shops;
            $overdue = DB::table('ledgers')->join('ledger_balance','ledgers.id','=','ledger_balance.ledger_id')
                ->where('ledgers.project_id','=',$p->id)->sum('balance') ?? 0;
            $collection_this_month = DB::table('revenues')->where('project_id','=',$p->id)
                ->where(DB::raw("CONCAT(YEAR(collection_date),MONTH(collection_date))"),'=',date('Yn'))
                ->sum('amount') ?? 0;

            /*getting past 12 months revenue*/
            $range = "'" . date('Yn', strtotime("-1year")) . "' AND '" . date('Yn') . "'";
            $sql = "SELECT SUM(amount) amount, CONCAT(YEAR(collection_date),MONTH(collection_date)) mnth
                    FROM revenues
                    WHERE  project_id = {$p->id}  AND CONCAT(YEAR(collection_date),MONTH(collection_date)) BETWEEN {$range}
                    GROUP BY mnth";
            $rev_data = DB::select(DB::raw($sql));
            $dat = [];
            foreach($rev_data as $rd){
                $dt = date_create_from_format('Yn',$rd->mnth)->format("M 'y");
                $dat[] = ['y' => $rd->amount, 'label' => $dt ];
            }
            /*dummy data*/
            /*for($i = 11; $i >= 0; $i--){
                $dat[] = ['y' => rand(10000,100000), 'label' => date("M 'y", strtotime("-{$i}month")) ];
            }*/
            $projects_info[] = [
                'project_id' => $p->id,
                'project_name' => $p->name,
                'shops' => $no_of_shops,
                'vacant_shops' => $vacant_shops,
                'overdue' => $overdue,
                'collection_this_month' => $collection_this_month,
                'rev_data' => json_encode($dat)
            ];
        }
        return view('home', compact('no_of_projects','clients','shops','projects_info'));
    }

    private static function staffDashboard($req){
        /*getting all overdue payments*/
        $payments = DB::table('revenues')->whereNull('invoice_id')->get();
        if($invoice_id = $req->session()->get('invoice_id')){
            return view('staff.dashboard',['payments' => $payments, 'invoice_id' => $invoice_id]);
        }
        return view('staff.dashboard',['payments' => $payments]);
    }

    private function dashBoardData(){
        /*this month's revenue*/

    }
}
