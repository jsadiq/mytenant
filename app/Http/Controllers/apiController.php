<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class apiController extends Controller
{
    public function index($action){
        return $this->$action();
    }

    public function getShops(){
        if($_POST['shop_type'] == 'sold'){
            $res = DB::table('shops')->where([
                ['project_id', '=', $_POST['project_id']],
                ['type', '=', 'for_sale']
            ])->whereNotNull('tenant_id')->get();
        }
        if($_POST['shop_type'] == 'for_sale'){
            $res = DB::table('shops')->where([
                ['project_id', '=', $_POST['project_id']],
                ['type', '=', 'for_sale']
            ])->whereNull('tenant_id')->get();
        }
        return response()->json(['success' => 1, 'shops' => $res]);
    }

    public function getShopData(){
        $tenant_id = $_POST['tenant_id'];
        $shop_id = $_POST['shop_id'];
        $shop = DB::table('shops')->select(['rent','service_charge','tenant_id'])->where('id','=',$shop_id)->first();
        $tenant = DB::table('tenants')->where('id', '=', $tenant_id)->first();
        $overdue_service_charge = DB::table('revenues')->selectRaw('id, amount, due_date, date_format(due_date, "%d %M, %Y") due_date_formatted')->where(array(
            ['shop_id','=',$shop_id],
            ['revenue_type','=','service charge'],
        ))->whereNull('collected_by')->get();
        $overdue_installment = DB::table('revenues')->selectRaw('id, amount, due_date, date_format(due_date, "%d %M, %Y") due_date_formatted')->where(array(
            ['shop_id','=',$shop_id],
            ['revenue_type','=','installment'],
        ))->whereNull('collected_by')->orderBy('id', 'asc')->first();
        $potential_buyers = DB::table('tenants')->where('id','<>',$shop->tenant_id)->get();
        return response()->json([
            'success' => 1,
            'shop' => $shop,
            'tenant' => $tenant,
            'overdue_service_charge' => $overdue_service_charge,
            'overdue_installment' => $overdue_installment,
            'potential_buyers' => $potential_buyers
        ]);
    }

    public function getFilteringOption(){
        $tenants = DB::table('shops')
                ->join("shop_attributes","shops.id", "=", "shop_attributes.shop_id")
                ->join("tenants", "tenants.id", "=", "shop_attributes.attribute_value")
                ->where([
                    ["attribute_name","=","tenant"],
                    ["shops.project_id","=",$_POST["project_id"]],
                ])->selectRaw("DISTINCT attribute_value id, tenants.name, tenants.phone")->get();
        $shops = DB::table('shops')->select(['id','shop_no'])->where('project_id','=',$_POST['project_id'])->get();
        return response()->json([
            'tenants' => $tenants,
            'shops' => $shops
        ]);
    }

}
