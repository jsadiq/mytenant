<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\Auth;

class LedgerController extends Controller
{
    private  $projects = [];
    public function __construct(){
        $this->projects = DB::table('projects')->get();
    }
    public function createBooking(Request $req){
        $shops = [];
        $tenants = DB::table('tenants')->get(['id','name','phone']);
        if($req->input('project')){
            $shops = DB::table('shops')->where('project_id',$req->input('project'))->get(['id','shop_code','price']);
        }
        return view('ledger/booking',[
            'projects' => $this->projects,
            'shops' => $shops,
            'tenants' => $tenants
        ]);
    }
    public function saveBooking(Request $req){
        try{
            $validation_status = $this->validate_booking_request($req->all());
            if($validation_status['status'] == 'error'){
                throw new Exception($validation_status['msg']);
            }
            $invoice_id = $this->generate_invoice_id($req->input('project_id'));
            $this->create_booking($req->all(), $invoice_id);

            return redirect(url('/?invoice_no='.$invoice_id));

        }catch (Exception $e){
            return redirect(route('booking'))->with('error', $e->getMessage());
        }catch(QueryException $e){
            return redirect(route('booking'))->with('error', $e->getMessage());
        }
    }
    public function createDeed(Request $req){
        $shops = [];
        $tenants = DB::table('tenants')->get(['id','name','phone']);
        $transfer_fee = DB::table('settings')->where('settings_name', 'transfer_fee')->first()->value;
        $adv_service_charge = DB::table('settings')->where('settings_name', 'service_charge_advance')->first()->value;
        if($req->input('project')){
            $shops = DB::table('shops')->where('project_id',$req->input('project'))->get(['id','shop_code','price','service_charge']);
        }
        return view('ledger/deed',[
            'projects' => $this->projects,
            'shops' => $shops,
            'tenants' => $tenants,
            'transfer_fee' => $transfer_fee,
            'adv_service_charge' => $adv_service_charge
        ]);
    }
    public function saveDeed(Request $req){
        try{
            $validation_status = $this->validate_deed_request($req->all());
            if($validation_status['status'] == 'error'){
                throw new Exception($validation_status['msg']);
            }
            $invoice_id = $this->generate_invoice_id($req->input('project_id'));
            $this->create_deed($req->all(), $invoice_id);

            return redirect(url('/?invoice_no='.$invoice_id));

        }catch (Exception $e){
            return redirect(route('deed'))->with('error', $e->getMessage());
        }catch(QueryException $e){
            return redirect(route('deed'))->with('error', $e->getMessage());
        }
    }
    public function ledgerList(Request $req, $ledger_type){
        $ledgers = [];
        if($req->input('project')){
            $is_active = ' AND ledgers.is_active = 1 ';
            if($req->input('show_cancelled_ledgers')){
                $is_active = '';
            }
            $shops_sql = "SELECT id, GROUP_CONCAT(shop SEPARATOR '<br>') shops FROM (SELECT ledgers.id, CONCAT(GROUP_CONCAT(shops.shop_no), ' block-', blocks.name, ' ', floors.name) shop FROM ledgers JOIN ledger_shops ON ledgers.id = ledger_shops.ledger_id
JOIN shops ON ledger_shops.shop_id = shops.id JOIN floors ON floors.id = shops.floor_id JOIN blocks ON shops.block_id = blocks.id
GROUP BY ledgers.id, blocks.id) AS tbl2
GROUP BY id";
            $sql = "SELECT ledgers.id, ledger_no, ledgers.is_active, tbl.shops, tenants.name client, deeds.deed_no, ledger_balance.balance, sales.amount sales_price
FROM ledgers JOIN ledger_shops ON ledgers.id = ledger_shops.ledger_id JOIN shops ON shops.id = ledger_shops.shop_id JOIN floors ON floors.id = shops.floor_id JOIN blocks ON blocks.id = shops.block_id JOIN tenants ON ledgers.tenant_id = tenants.id
JOIN ledger_balance ON ledgers.id = ledger_balance.ledger_id
JOIN ({$shops_sql}) as tbl ON ledgers.id = tbl.id
LEFT JOIN deeds ON deeds.id = ledgers.deed_id
LEFT JOIN sales ON sales.ledger_id = ledgers.id
WHERE ledgers.project_id = {$req->input('project')} {$is_active} AND ledger_type = '{$ledger_type}'
GROUP BY ledgers.id";
            $ledgers = DB::select(DB::raw($sql));

        }
        return view('ledger/list',[
            'projects' => $this->projects,
            'ledger_type' => $ledger_type,
            'ledgers' => $ledgers
        ]);
    }
    private function validate_booking_request($post){
        $error_msg = [];
        /*is shop selected?*/
        if(!isset($post['shops']) || count($post['shops']) == 0){
            $error_msg[] = "No shop selected.";
        }else{
            /*are shops from same project?*/
            foreach($post['shops'] as $sid){
                $project_id = DB::table('shops')->select(['project_id'])->where('id','=',$sid)->first();
                if($project_id->project_id != $post['project_id']){
                    $error_msg[] = "Shop not found.";
                }
            }
            /*are shops available for sale? they should not have any active ledger*/
            foreach($post['shops'] as $sid){
                $ledger = DB::table('ledger_shops')->leftJoin('shops','shops.id','=','ledger_shops.shop_id')->leftJoin('ledgers','ledgers.id','=','ledger_shops.ledger_id')->select(['shops.shop_code'])->where([['shop_id',$sid],['ledgers.is_active',1]])->first();
                if($ledger){
                    $error_msg[] = "Shop {$ledger->shop_code} not available for sale.";
                }
            }
        }

        if(empty($post['down_payment'])){
            $error_msg[] = "Down payment is required.";
        }
        if(!empty($error_msg)){
            return [
                'status' => 'error',
                'msg' => implode('<br />', $error_msg)
            ];
        }
        return [
          'status' => 'success'
        ];
    }
    private function validate_deed_request($post){
        $error_msg = [];
        /*is shop selected?*/
        if(!isset($post['shops']) || count($post['shops']) == 0){
            $error_msg[] = "No shop selected.";
        }
        /*are shops from same project?*/
        foreach($post['shops'] as $sid){
            $project_id = DB::table('shops')->select(['project_id'])->where('id','=',$sid)->first();
            if($project_id->project_id != $post['project_id']){
                $error_msg[] = "Shop not found.";
            }
        }
        /* shops must be under same active ledger */
        $ledgers = DB::table('ledger_shops')
            ->join('ledgers','ledgers.id','=','ledger_shops.ledger_id')
            ->whereIn('ledger_shops.shop_id',$post['shops'])->where([['ledgers.is_active','=',1],['ledgers.ledger_type','=','shop']])->select(['ledgers.id'])->get();
        if(count($ledgers) != count($post['shops'])){
            $error_msg[] = "All shops don't have ledgers.";
        }
        if(count($post['shops'])>2){
            for($i=0; $i < count($post['shops'])-1; $i++){
                if($ledgers[$i]->id != $ledgers[$i+1]->id){
                    $error_msg[] = "All shops must be in same ledger.";
                }
            }
        }
        /*is client selected?*/
        if(empty($post['tenant'])){
            $error_msg[] = "Select a client.";
        }
        /*do shops have any due?*/
        $sql = "SELECT SUM(balance) balance  FROM ledger_balance JOIN ledgers ON ledger_balance.ledger_id = ledgers.id
JOIN ledger_shops ON ledger_shops.ledger_id = ledgers.id
WHERE ledger_shops.shop_id IN (" . implode(',',$post['shops']). ") AND ledgers.is_active = 1";

        $balance = DB::select(DB::raw($sql));

        if($balance && $balance[0]->balance != 0){
            $error_msg[] = "Shop has due.";
        }

        if(isset($post['collect_transfer_fee']) && $post['collect_transfer_fee'] == 1){
            $current_ledger = $this->get_current_ledger($post['shops']);
            if($current_ledger){
                $deed_id = DB::table('ledgers')->where('ledgers.id','=',$current_ledger)->first()->deed_id;
                if(!$deed_id){
                    $error_msg[] = "Shop is not transferable.";
                }
            }else{
                $error_msg[] = "Shop is not transferable.";
            }
        }

        if(!empty($error_msg)){
            return [
                'status' => 'error',
                'msg' => implode('<br />', $error_msg)
            ];
        }
        return [
            'status' => 'success'
        ];
    }
    public function showLedger($ledger_id){
        $project = DB::table('projects')->join('ledgers','ledgers.project_id','=','projects.id')->select(['projects.*'])->where('ledgers.id','=',$ledger_id)->first();
        $ledger = DB::table('ledgers')->where('id','=',$ledger_id)->first();
        $shop = DB::table('shops')->join('ledger_shops','ledger_shops.shop_id','=','shops.id')->join('floors','floors.id','=','shops.floor_id')->join('blocks','blocks.id','=','shops.block_id')
            ->select(DB::raw("floors.name floor,blocks.name block,GROUP_CONCAT(shops.shop_no) shop_no"))
            ->where('ledger_shops.ledger_id','=',$ledger_id)
            ->groupBy('blocks.id')
            ->first();
        $client = DB::table('tenants')->join('ledgers','ledgers.tenant_id','=','tenants.id')->where('ledgers.id','=',$ledger_id)->select(['tenants.*'])->first();
        $service_charge = DB::table('shops')->join('ledger_shops','ledger_shops.shop_id','=','shops.id')->where('ledger_id',$ledger_id)->sum('service_charge');
        $ledger_entries = DB::table('ledger_entries')->where('ledger_id','=',$ledger_id)->get();
        return  view('ledger/view',compact('ledger','project','shop','client','service_charge','ledger_entries'));
    }

    public function collectPayment(Request $req){
        $clients = DB::table('tenants')->get();
        $dues = [];
        if($req->get('client')){
            $shops_sql = "(SELECT id, GROUP_CONCAT(shop SEPARATOR '<br>') shops FROM (SELECT ledgers.id, CONCAT(GROUP_CONCAT(shops.shop_no), ' block-', blocks.name, ' ', floors.name) shop FROM ledgers JOIN ledger_shops ON ledgers.id = ledger_shops.ledger_id
JOIN shops ON ledger_shops.shop_id = shops.id JOIN floors ON floors.id = shops.floor_id JOIN blocks ON shops.block_id = blocks.id
GROUP BY ledgers.id, blocks.id) AS tbl2
GROUP BY id) shop";
            $dues = DB::table('ledgers')
                ->join(DB::raw($shops_sql),'shop.id','=','ledgers.id')
                ->join('ledger_balance','ledger_balance.ledger_id','=','ledgers.id')
                ->join('projects','projects.id','=','ledgers.project_id')
                ->where('ledgers.tenant_id','=',$req->get('client'))
                ->where('ledgers.is_active','=',1)
                ->get();
        }
        if($req->isMethod('post')){
            $post = $req->input();
            $inv_id = $this->generate_invoice_id($post['project_id']);
            $this->create_ledger_entry($post['ledger_id'],$post['amount'],'received',$post['note']);
            /*getting shops*/
            $shops = [];
            $shops_res = DB::table('ledger_shops')->where('ledger_id','=',$post['ledger_id'])->get();
            foreach($shops_res as $s){
                $shops[] = $s->shop_id;
            }
            $rev_type = ($post['ledger_type'] == 'shop') ? 'shop price' : 'service charge';
            /*adding data to revenue table*/
            $this->add_revenue($post['project_id'], $post['ledger_id'], $shops,  $rev_type,$post['amount'], $post['client'], $inv_id);
            return redirect(url('/?invoice_no='.$inv_id));
        }
        return view('ledger/payment',compact('clients','dues'));
    }

    public function revenueList( Request $req ){
        $revenues = [];
        if($req->input('project') && $req->input('to') && $req->input('from')){
            $shops_sql = "(SELECT id, GROUP_CONCAT(shop SEPARATOR '<br>') shops FROM (SELECT ledgers.id, CONCAT(GROUP_CONCAT(shops.shop_no), ' block-', blocks.name, ' ', floors.name) shop FROM ledgers JOIN ledger_shops ON ledgers.id = ledger_shops.ledger_id
JOIN shops ON ledger_shops.shop_id = shops.id JOIN floors ON floors.id = shops.floor_id JOIN blocks ON shops.block_id = blocks.id
GROUP BY ledgers.id, blocks.id) AS tbl2
GROUP BY id) as ledger_shops";
            $revenues = DB::table('revenues')
                ->join(DB::raw($shops_sql),'ledger_shops.id','=','revenues.ledger_id')
                ->join('tenants','tenants.id','=','revenues.tenant_id')
                ->where('revenues.project_id','=',$req->input('project'))
                ->whereBetween('collection_date',[$req->input('from'),$req->input('to')])
                ->select(['ledger_shops.shops','tenants.name','tenants.phone','revenue_type','amount','collection_date'])
                ->orderBy('collection_date')
                ->get();
        }
        return view('ledger/revenue_report',[
            'projects' => $this->projects,
            'revenues' => $revenues
        ]);
    }
    public function overdueList( Request $req ){
        $overdue = [];
        if($req->input('project')){
            $shops_sql = "(SELECT id, GROUP_CONCAT(shop SEPARATOR '<br>') shops FROM (SELECT ledgers.id, CONCAT(GROUP_CONCAT(shops.shop_no), ' block-', blocks.name, ' ', floors.name) shop FROM ledgers JOIN ledger_shops ON ledgers.id = ledger_shops.ledger_id
JOIN shops ON ledger_shops.shop_id = shops.id JOIN floors ON floors.id = shops.floor_id JOIN blocks ON shops.block_id = blocks.id
GROUP BY ledgers.id, blocks.id) AS tbl2
GROUP BY id) as ledger_shops";
            $overdue = DB::table('ledgers')
                ->join('ledger_balance','ledger_balance.ledger_id','=','ledgers.id')
                ->join(DB::raw($shops_sql),'ledger_shops.id','=','ledgers.id')
                ->join('tenants','tenants.id','=','ledgers.tenant_id')
                ->where('ledgers.project_id','=',$req->input('project'))
                ->where('ledgers.is_active','=',1)
                ->where('ledger_balance.balance','>',0)
                ->select(['ledger_shops.shops','tenants.name','tenants.phone','balance','ledger_type'])
                ->get();
        }
        return view('ledger/overdue_report',[
            'projects' => $this->projects,
            'overdue' => $overdue
        ]);
    }
    private function create_booking($post, $invoice_id){
        DB::transaction(function () use ($post, $invoice_id) {
            /*open a new ledger*/
            $payment_cleared = 0;
            $total_price = DB::table('shops')->whereIn('id',$post['shops'])->sum('price');
            if($total_price == $post['down_payment']){
                $payment_cleared = 1;
            }
            $ledger_id = $this->create_ledger($post['project_id'], $post['tenant'],'shop',$post['shops'],$payment_cleared,$post['date']);

            /*ledger entry*/
            $this->create_ledger_entry($ledger_id,$total_price,'bill_amount','Sales price.',$post['date']);
            $this->create_ledger_entry($ledger_id,$post['down_payment'],'received',$post['note'],$post['date']);

            /*insert data in sales table*/
            $data_sales = array(
                'project_id' => $post['project_id'],
                'ledger_id' => $ledger_id,
                'tenant_id' => $post['tenant'],
                'amount' => $total_price,
                'down_payment' => $post['down_payment'],
                'sale_date' => $post['date'] ?? date('Y-m-d')
            );
            DB::table('sales')->insert($data_sales);

            /*adding down payment to revenues table*/
            $this->add_revenue($post['project_id'], $ledger_id, $post['shops'],'down payment', $data_sales['down_payment'], $post['tenant'], $invoice_id, $post['date']);
        });
    }

    private function create_deed($post, $invoice_id){
        DB::transaction(function () use ($post, $invoice_id) {
            /*if transfer fee is collected create a new shop ledger*/
            $client = $post['tenant'];
            if( isset($post['collect_transfer_fee']) && $post['collect_transfer_fee'] == 1 ){

                $current_ledger = $this->get_current_ledger($post['shops']);

                $shop_ledger = $this->create_ledger($post['project_id'],$post['tenant'],'shop',$post['shops'],1,$post['date']);

                $this->create_deed_for_remaining_shops($current_ledger, $post);

                /*cancel the current ledger*/
                DB::table('ledgers')->where('id',$current_ledger)->update(['is_active'=>0]);

            }else{
                $shop_ledger = DB::table('ledger_shops')->join('ledgers','ledgers.id','=','ledger_shops.ledger_id')
                                ->where([
                                    ['ledgers.ledger_type','=','shop'],
                                    ['ledgers.is_active','=',1],
                                    ['ledger_shops.shop_id','=',$post['shops'][0]],
                                ])->first()->ledger_id;
                DB::table('ledgers')->where('id',$shop_ledger)->update(['is_payment_cleared'=>1]);
                $client = DB::table('ledgers')->select('tenant_id')->where('id','=',$shop_ledger)->first()->tenant_id;
            }
            /*add data to deeds table*/
            $deed_value = DB::table('shops')->whereIn('id',$post['shops'])->sum('price');
            $deed_id = DB::table('deeds')->insertGetId([
                'project_id' => $post['project_id'],
                'deed_no' => $post['deed_no'],
                'value' => $deed_value,
                'tenant_id' => $client,
                'created_at' => $post['date'] ?? date('Y-m-d')
            ]);
            DB::table('ledgers')->where('id',$shop_ledger)->update(['deed_id'=>$deed_id]);

            /*ledger entries*/
            if( isset($post['collect_transfer_fee']) && $post['collect_transfer_fee'] == 1 ){
               /*adding transfer fee*/
                $transfer_fee = $post['transfer_fee'] ?? $this->get_transfer_fee($post['shops']);
                $this->create_ledger_entry($current_ledger,$transfer_fee,'bill_amount','Transfer fee.',$post['date']);
                $this->create_ledger_entry($current_ledger,$transfer_fee,'received',$post['transfer_fee_note'],$post['date']);
                /*adding data to revenue table*/
                $new_inv = $this->generate_invoice_id($post['project_id']);
                $this->add_revenue($post['project_id'], $current_ledger, $post['shops'],'transfer fee', $transfer_fee, $client, $new_inv,$post['date']);


            }
            /*create service charge ledger*/
            $service_charge_ledger = $this->create_ledger($post['project_id'],$client,'service_charge',$post['shops'],1,$post['date']);
            DB::table('ledgers')->where('id',$service_charge_ledger)->update(['deed_id'=>$deed_id]);

            /*adding service charge*/
            $service_charge = $this->get_adv_service_charge($post['shops']);
            $note = "Advance service charge";
            //$this->create_ledger_entry($service_charge_ledger, $service_charge, 'bill_amount' , $note, $post['date']);
            $this->create_ledger_entry($service_charge_ledger, $service_charge, 'received' , $post['service_charge_note'], $post['date']);
            /*adding data to revenue table*/
            $this->add_revenue($post['project_id'], $service_charge_ledger, $post['shops'],'service charge', $service_charge, $client, $invoice_id, $post['date']);
            /*creating the ledger entries if from and to date selected*/
            if($post['ledger_entry_from'] && $post['ledger_entry_to']){
                $total_amount = DB::table('shops')->whereIn('id',$post['shops'])->sum('service_charge');
                $date_diff = new \DateInterval("P1M");
                $entry_date = date_create_from_format('Y-m-d', $post['ledger_entry_from']);
                $ending_date = date_create_from_format('Y-m-d', $post['ledger_entry_to']);
                while($entry_date->format('Y-m') <= $ending_date->format('Y-m')){
                    $this->create_ledger_entry($service_charge_ledger,$total_amount,'bill_amount','Service charge',$entry_date->format('Y-m-d'));
                    //$this->create_ledger_entry($service_charge_ledger,$total_amount,'received','Service charge received',$entry_date->format('Y-m-d'));
                    //$this->add_revenue($post['project_id'], $service_charge_ledger, $post['shops'],'service charge', $total_amount, $client, $invoice_id, $post['date']);
                    $entry_date->add($date_diff);
                }
            }

        });

    }
    private function generate_invoice_id($project_id){
        $project_code = DB::table('projects')->where('id','=',$project_id)->first()->code;
        $invoice_id = $project_code . date('ymdHi') . strtoupper(substr(md5(microtime()),rand(0,26),2));
        return $invoice_id;
    }

    private function get_ledger_balance($ledger_id){
        return DB::table('ledger_balance')->select('balance')
            ->where('ledger_id',$ledger_id)
            ->first()->balance ?? 0;
    }
    private function get_transfer_fee($shops){
        if($_POST['transfer_fee']){
            return $_POST['transfer_fee'];
        }
        $transfer_fee = DB::table('settings')->where('settings_name', 'transfer_fee')->first()->value;
        $total_price = DB::table('shops')->whereIn('id', $shops)->sum('price');
        return $total_price * $transfer_fee / 100;
    }
    private function create_ledger($project_id,$tenant_id,$ledger_type,$shops,$is_payment_cleared=1,$date=null){
        /*if service charge ledger, cancel the old ledger*/
        if($ledger_type == 'service_charge'){
            DB::table('ledgers')->join('ledger_shops','ledgers.id','=','ledger_shops.ledger_id')
                ->whereIn('ledger_shops.shop_id',$shops)->where('ledgers.ledger_type','=',$ledger_type)
                ->update(['ledgers.is_active' => 0]);
        }
        /*if shop ledger cancel the ledger if it doesn't have any shop left*/
        /*$old_ledger = DB::table('ledgers')->join('ledger_shops','ledger_shops.ledger_id','=','ledgers.id')
            ->whereNotIn('shop_id',$shops)->where('ledgers.ledger_type','=',$ledger_type)->select('ledgers.id')->first();
        if(!$old_ledger){
            DB::table('ledgers')->join('ledger_shops','ledgers.id','=','ledger_shops.ledger_id')
                ->whereIn('ledger_shops.shop_id',$shops)->where('ledgers.ledger_type','=',$ledger_type)
                ->update(['ledgers.is_active' => 0]);
        }*/

        $nextId = DB::table('ledgers')->max('id') + 1;
        $ledger_data = [
            'project_id'    => $project_id,
            'ledger_no'     => date('ymd').str_pad($nextId,6,'0',STR_PAD_LEFT),
            'ledger_type'   => $ledger_type,
            'tenant_id'     => $tenant_id,
            'is_active'     => 1,
            'is_payment_cleared' => $is_payment_cleared,
            'date_created'   => $date ?? date('Y-m-d H:i:s')
        ];
        $ledger_id = DB::table('ledgers')->insertGetId($ledger_data);
        /*insert data in ledger_shops table*/
        $ledger_shops_data = [];
        foreach($shops as $sid){
            $ledger_shops_data[] = [
                'ledger_id' => $ledger_id,
                'shop_id' => $sid,
            ];
        }
        DB::table('ledger_shops')->insert($ledger_shops_data);

        /*insert data in ledger_balance table*/
        DB::table('ledger_balance')->insert(
            [
                'project_id'    => $project_id,
                'ledger_id'     => $ledger_id,
                'tenant_id'     => $tenant_id,
                'balance'       => 0
            ]
        );
        return $ledger_id;
    }

    private function create_ledger_entry($ledger_id, $amount, $type, $desc = '', $date = null){
        $balance = $this->get_ledger_balance($ledger_id);
        $data = array(
            'ledger_id' => $ledger_id,
            'date' => $date ?? date("Y-m-d H:i:s"),
            'description' => $desc,
        );
        if($type == 'bill_amount'){
            $data['bill_amount'] = $amount;
            $data['balance'] = $balance + $amount;
        }else{
            $data['received'] = $amount;
            $data['balance'] = $balance - $amount;
        }
        DB::table('ledger_balance')->where('id',$ledger_id)->update(['balance' => $data['balance']]);
        return DB::table('ledger_entries')->insertGetId($data);
    }

    private function get_adv_service_charge($shops){
        if($_POST['adv_service_charge']){
            return $_POST['adv_service_charge'];
        }
        $months = DB::table('settings')->where('settings_name', 'service_charge_advance')->first()->value;
        $s_charge = DB::table('shops')->whereIn('id',$shops)->sum('service_charge');
        return $s_charge * $months;
    }

    private function add_revenue($project_id, $ledger_id, $shops, $rev_type, $amount, $tenant, $inv_id, $date = null){
        $sql = "SELECT GROUP_CONCAT('Shop No. ',shops.shop_no,', Block ',blocks.name,', ', floors.name) as shop FROM
                    shops JOIN blocks ON shops.id = blocks.id JOIN floors ON shops.floor_id = floors.id
                    WHERE shops.id IN (" . implode(',',$shops) . ")
                    GROUP BY shops.id";
        $shops_info = DB::select(DB::raw($sql));
        DB::table('revenues')->insert(array(
            'project_id' => $project_id,
            'ledger_id' => $ledger_id,
            'shops' => implode('. ', array_map(function($item){
                return $item->shop;
            },$shops_info)),
            'revenue_type' => $rev_type,
            'amount' => $amount,
            'collection_date' => $date ?? date('Y-m-d'),
            'tenant_id' => $tenant,
            'collected_by' => Auth::id(),
            'invoice_id' => $inv_id
        ));
    }

    public function createEntries(Request $req){

        if($req->isMethod('post')){
            $project   = $req->input('project');
            $ledger_no = $req->input('ledger_no');
            $date = $req->input('date');
            $bill_amount = $req->input('bill_amount');
            $collect_amount = $req->input('collect_amount');
            $particulars = $req->input('particulars');

            $entries = 0;
            for($i = 0; $i < count($project); $i++){
                if($project[$i] && $ledger_no[$i] && $date[$i] && ($bill_amount[$i] || $collect_amount[$i]) && $particulars[$i]){
                    $data = array(
                        'project' => $project[$i],
                        'ledger_no' => $ledger_no[$i],
                        'date' => $date[$i],
                        'bill_amount' => $bill_amount[$i],
                        'collect_amount' => $collect_amount[$i],
                        'particulars' => $particulars[$i]
                    );
                    DB::transaction(function() use ($data, &$entries){
                        $inv_id = $this->generate_invoice_id($data['project']);
                        $ledger= DB::table('ledgers')->where('ledger_no','=',$data['ledger_no'])->first();
                        $type = ($data['bill_amount']) ? 'bill_amount' : 'received';
                        $amount = $data['bill_amount'] ?? $data['collect_amount'];
                        $this->create_ledger_entry($ledger->id,$amount,$type,$data['particulars'],$data['date']);
                        /*getting shops*/
                        $shops = [];
                        $shops_res = DB::table('ledger_shops')->where('ledger_id','=',$ledger->id)->get();
                        foreach($shops_res as $s){
                            $shops[] = $s->shop_id;
                        }
                        if($type == 'received'){
                            $this->add_revenue($data['project'], $ledger->id, $shops,  'service charge',$amount, $ledger->tenant_id, $inv_id, $data['date']);
                        }
                        $entries++;
                    });
                }
            }
            return redirect(route('create-ledger-entries'))->with('success', $entries . " entries created.");
        }

        return view('ledger/create_entries',['projects' => $this->projects]);
    }

    public function deleteLedger( Request $req ){
        if($req->isMethod('post')){
            $to_delete = [];
            $ledger_no = $req->input('ledger_no');
            $ledger = DB::table('ledgers')->where('ledger_no','=',$ledger_no)->first();
            $to_delete[] = $ledger->id;
            $deed_id = 0;
            /*if it is a shop ledger we will delete all service charge ledgers */
            if($ledger->ledger_type == 'shop' && $ledger->deed_id){
                $s_ledgers = DB::table('ledgers')->where('deed_id','=',$ledger->deed_id)->where('ledger_type','=','service_charge')->get();
                $deed_id = $ledger->deed_id;
                foreach($s_ledgers as $sl){
                    $to_delete[] = $sl->id;
                }
            }
            try{
                DB::transaction(function() use ($to_delete, $deed_id){
                    foreach($to_delete as $ledger_id){
                        DB::table('ledgers')->where('id','=',$ledger_id)->delete();
                        DB::table('ledger_shops')->where('ledger_id','=',$ledger_id)->delete();
                        DB::table('ledger_entries')->where('ledger_id','=',$ledger_id)->delete();
                        DB::table('ledger_balance')->where('ledger_id','=',$ledger_id)->delete();
                        DB::table('sales')->where('ledger_id','=',$ledger_id)->delete();
                        DB::table('revenues')->where('ledger_id','=',$ledger_id)->delete();
                    }
                    if($deed_id){
                        DB::table('deeds')->where('id','=',$deed_id)->delete();
                    }
                });
            }catch (Exception $e){
                return redirect(route('delete-ledger'))->with('error', $e->getMessage());
            }

            return redirect(route('delete-ledger'))->with('success', count($to_delete) . " ledgers deleted.");

        }
        return view('ledger/delete');
    }

    public function createOtherLedger(Request $req){
        $shops = [];
        $tenants = DB::table('tenants')->get(['id','name','phone']);
        /*form submitted to create*/
        if($req->isMethod('post')){
            $post = $req->input();
            /*create ledger*/
            $invoice_id = $this->generate_invoice_id($post['project_id']);
            $ledger = $this->create_ledger($post['project_id'],$post['tenant'],'other',$post['shops'],1,$post['date']);
            if($post['advance']){
                $this->create_ledger_entry($ledger,$post['advance'],'bill_amount','Advance',$post['date']);
                $this->create_ledger_entry($ledger,$post['advance'],'received','Advance',$post['date']);
                $this->add_revenue($post['project_id'], $ledger, $post['shops'],'service charge', $post['advance'], $post['tenant'], $invoice_id, $post['date']);
            }
            /*creating the ledger entries if from and to date selected*/
            if($post['ledger_entry_from'] && $post['ledger_entry_to']){
                $total_amount = DB::table('shops')->whereIn('id',$post['shops'])->sum('service_charge');
                $date_diff = new \DateInterval("P1M");
                $entry_date = date_create_from_format('Y-m-d', $post['ledger_entry_from']);
                $ending_date = date_create_from_format('Y-m-d', $post['ledger_entry_to']);
                while($entry_date->format('Y-m') <= $ending_date->format('Y-m')){
                    $this->create_ledger_entry($ledger,$total_amount,'bill_amount','Service charge',$entry_date->format('Y-m-d'));
                    //$this->create_ledger_entry($ledger,$total_amount,'received',$post['service_charge_note'],$entry_date->format('Y-m-d'));
                    //$this->add_revenue($post['project_id'], $ledger, $post['shops'],'service charge', $total_amount, $post['tenant'], $invoice_id, $post['date']);
                    $entry_date->add($date_diff);
                }
            }
            return redirect(route('ledgers',['ledger_type'=>'other']))->with('success',"ledger created.");
        }
        if($req->input('project')){
            $shops = DB::table('shops')
                ->join('blocks','shops.block_id','=','blocks.id')
                ->where('shops.project_id',$req->input('project'))
                ->where('blocks.name','=','Ext-')
                ->get(['shops.id','shop_code','price','service_charge']);
        }
        return view('ledger/create_other_ledger',[
            'projects' => $this->projects,
            'shops' => $shops,
            'tenants' => $tenants,
        ]);
    }

    private function get_current_ledger($shops){
        $ledger = DB::table('ledger_shops')
            ->join('ledgers','ledgers.id','=','ledger_shops.ledger_id')
            ->where('ledger_shops.shop_id','=',$shops[0])
            ->where('ledgers.is_active','=',1)->first(['ledgers.id']);
        return $ledger->id ?? null;
    }
    private function create_deed_for_remaining_shops($ledger, $post){
        $client = DB::table('ledgers')->select('tenant_id')->where('id','=',$ledger)->first()->tenant_id;
        /*getting remaining shops*/
        $shops = [];
        $ledger_shops = DB::table('ledger_shops')->where('ledger_id','=',$ledger)->get(['shop_id']);
        foreach($ledger_shops as $ledger_shop){
            if(!in_array($ledger_shop->shop_id,$post['shops'])){
                $shops[] = $ledger_shop->shop_id;
            }
        }
        if(count($shops) == 0) return; //there is no remaining shop
        $date = $post['date'] ?? date('Y-m-d');
        $deed_id = DB::table('deeds')->insertGetId([
            'project_id' => $post['project_id'],
            'deed_no' => time(),
            'value' => 0,
            'tenant_id' => $client,
            'created_at' => $date
        ]);
        /*creating shop ledger*/
        $shop_ledger = $this->create_ledger($post['project_id'],$client,'shop',$shops, 1, $date);
        DB::table('ledgers')->where('id',$shop_ledger)->update(['deed_id'=>$deed_id]);
        /*creating service charge ledger*/
        $service_charge_ledger = $this->create_ledger($post['project_id'],$client,'service_charge',$shops,1,$date);
        DB::table('ledgers')->where('id',$service_charge_ledger)->update(['deed_id'=>$deed_id]);
    }
}
