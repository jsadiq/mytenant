<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    private  $projects = [];
    public function __construct(){
        $this->projects = DB::table('projects')->get();
    }
    public function shopList(Request $req){
        $shops = [];
        //$floors = [];
        $blocks = [];
        $tenants = [];
        if($req->input('project')){
            $shops = DB::table('shops')
                ->join('floors','floors.id','=','shops.floor_id')
                ->join('blocks','blocks.id','=','shops.block_id')
                ->leftJoin('ledger_shops','ledger_shops.shop_id','=','shops.id')
                ->leftJoin('ledgers','ledgers.id','=','ledger_shops.ledger_id')
                ->leftJoin('tenants','tenants.id','=','ledgers.tenant_id')
                ->select('shops.*','tenants.id as tenant_id','tenants.name as tenant','tenants.phone as tenant_phone', 'blocks.id as block_id', 'blocks.name as block', 'floors.id as floor_id', 'floors.name as floor')
                ->where('shops.project_id','=',$req->input('project'))
                ->where(function ($query) {
                    $query->where('ledgers.is_active', '=', 1)
                        ->orWhere('ledgers.id','=',null);
                })
                ->groupBy(['shops.id'])
                ->get();
            //$floors = DB::table('floors')->where('project_id', '=', $req->input('project'))->get();
            $blocks = DB::table('floors')->join('blocks','floors.id','=','blocks.floor_id')
                ->where('floors.project_id', '=', $req->input('project'))
                ->select(DB::raw("CONCAT(floors.id,'#',blocks.id) id, CONCAT(floors.name, ' Block: ', blocks.name) name"))
                ->get();

            $tenants = DB::table('tenants')->get();
        }
        return view('shop/list',[
            'projects'=>$this->projects,
            'shops' => $shops,
            'floors' => $blocks,
            //'blocks' => $blocks,
            'tenants' => $tenants,
        ]);
    }

    public function save(Request $req){
        /*check if all fields are there*/
        if(empty($req->input('number')) || empty($req->input('floor')) || empty($req->input('service_charge')) || empty($req->input('size'))){
            return redirect(route('shop-list', ['project' => $req->input('project')]))->with('error', 'Fill up the required fields.');
        }
        $data = $req->input();
        $shop_code = $this->get_shop_code($data);
        /** does the shop number exist */
        if(!$req->input('id') && DB::table('shops')->where('shop_code', $shop_code)->exists()){
            return redirect(route('shop-list', ['project' => $req->input('project')]))->with('error', 'Duplicate shop.');
        }
        /** valid size and amount */
        if(!ctype_digit(str_replace('.','',$data['amount']))){
            return redirect(route('shop-list', ['project' => $req->input('project')]))->with('error', 'Not a valid price/rent.');
        }
        if(!ctype_digit(str_replace('.','',$data['size']))){
            return redirect(route('shop-list', ['project' => $req->input('project')]))->with('error', 'Not a valid size.');
        }
        if(!empty($data['service_charge']) && !ctype_digit(str_replace('.','',$data['service_charge']))){
            return redirect(route('shop-list', ['project' => $req->input('project')]))->with('error', 'Not a valid amount.');
        }
        $shop_attributes = array();
        $info = array(
            'project_id' => $data['project'],
            'floor_id' => explode('#',$data['floor'])[0],
            'block_id' => explode('#',$data['floor'])[1],
            'shop_no' => $data['number'],
            'shop_code' => $shop_code,
            'size' => $data['size'],
            'type' => 'for_sale',
        );
        if($req->input('id')){
            $shop = DB::table('shops')->where('id','=', $req->input('id'))->first();
        }
        if((isset($shop) && $shop->price != $data['amount']) || !isset($shop)){
            $info['price'] = $shop_attributes['price'] = $data['amount'];
        }
        if((isset($shop) && $shop->service_charge != $data['service_charge']) || !isset($shop)){
            $info['service_charge'] = $shop_attributes['service_charge'] = $data['service_charge'];
        }
        try{
            if($req->input('id')){
                $shop_id = $req->input('id');
                DB::table('shops')->where('id','=',$req->input('id'))->update($info);
                $msg = "Record updated.";
            }else{
                $shop_id = DB::table('shops')->insertGetId($info);
                $msg = "New shop created.";
            }
            foreach($shop_attributes as $att => $val){
                DB::table('shop_attributes')->insert(['shop_id' => $shop_id, 'attribute_name' => $att, 'attribute_value' => $val, 'updated_by' => Auth::id()]);
            }
        }catch (\Illuminate\Database\QueryException $e){
            return redirect(route('shop-list', ['project' => $req->input('project')]))->with('error', 'There was an error updating the record. Error' . $e->getMessage());
        }
        return redirect(route('shop-list', ['project' => $req->input('project')]))->with('success', $msg);

    }

    public function update_amount(Request $req){
        if($req->post('id')){
            $ids = explode(',', $req->post('id'));
            foreach($ids as $id){
                $shop = DB::table('shops')->where('id','=',$id)->first();
                $data = array();
                if($req->post('service_charge') && ctype_digit(str_replace('.','',$req->post('service_charge')))){
                    $update_type = $req->post('service_charge_update_type');
                    if($update_type == 'percent' && $shop->type == 'for_sale'){
                        $data['service_charge'] = $shop->service_charge + $shop->service_charge * $req->post('service_charge')/100;
                    }
                    if($update_type == 'percent' && $shop->type == 'for_rent'){
                        $data['rent'] = $shop->rent + $shop->rent * $req->post('service_charge')/100;
                    }
                    if($update_type == 'taka' && $shop->type == 'for_sale'){
                        $data['service_charge'] = $shop->service_charge + $req->post('service_charge');
                    }
                    if($update_type == 'taka' && $shop->type == 'for_rent'){
                        $data['rent'] = $shop->rent + $req->post('service_charge');
                    }
                }
                if($req->post('price') && ctype_digit(str_replace('.','',$req->post('price'))) && $shop->type == 'for_sale' && $req->post('price_update_type') == 'percent'){
                    $data['price'] = $shop->price + $shop->price * $req->post('price') / 100;
                }
                if($req->post('price') && ctype_digit(str_replace('.','',$req->post('price'))) && $shop->type == 'for_sale' && $req->post('price_update_type') == 'taka'){
                    $data['price'] = $shop->price + $req->post('price');
                }
                DB::table('shops')->where('id','=',$shop->id)->update($data);

                foreach($data as $attribute => $val){
                    DB::table('shop_attributes')->insert(
                        ['shop_id' => $shop->id, 'attribute_name' => $attribute, 'attribute_value' => $val, 'updated_by' => Auth::id()]);
                }
            }
            return redirect(route('shop-list', ['project' => $shop->project_id]))->with('success', "Shop information updated.");
        }
    }

    private function get_shop_code($data){
        $info = array(
            'project_id' => $data['project'],
            'floor_id' => explode('#',$data['floor'])[0],
            'block_id' => explode('#',$data['floor'])[1],
            'shop_no' => $data['number'],
        );
        $project_code = DB::table('projects')->where('id', $info['project_id'])->select('code')->first()->code;
        $floor_code = DB::table('floors')->select('code')->where('id', $info['floor_id'])->first()->code;
        $block_name = DB::table('blocks')->select('name')->where('id', $info['block_id'])->first()->name;
        return trim($project_code).trim($floor_code).trim($block_name).strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data['number'])));
    }
}
