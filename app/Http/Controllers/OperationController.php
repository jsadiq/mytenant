<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OperationController extends Controller
{
    private  $projects = [];
    public function __construct(){
        $this->projects = DB::table('projects')->get();
    }
    public function collectServiceCharge(Request $req){
        $invoice_id = '';
        if($req->isMethod('post')){
            $data = json_decode($_POST['data']);
            $invoice_id = $this->generate_invoice_id($data->shop_id);
            foreach($data->payments as $payment){
                $payment_info = DB::table('revenues')->select(['shop_id', 'revenue_type', 'invoice_id'])->where('id','=',$payment->overdue_id)->first();
                if($payment_info->shop_id != $data->shop_id || $payment_info->revenue_type != 'service charge' ||  $payment_info->invoice_id != null){
                    die("error");
                }
            }
            $user_id = Auth::id();
            foreach($data->payments as $payment){
                $update_data = [
                    'collected_by' => $user_id,
                    'collection_date' => date('Y-m-d'),
                    'invoice_id' => $invoice_id
                ];
                DB::table('revenues')->where('id', '=', $payment->overdue_id)->update($update_data);
            }
        }
        return view('staff/collect_service_charge',[
            'projects' => $this->projects,
            'invoice_id' => $invoice_id
        ]);
    }

    public function show_invoice($invoice_id){
        $sql = "SELECT revenues.ledger_id, details.*, tenants.name tenant_name, tenants.phone tenant_phone, revenues.revenue_type, revenues.amount
                FROM revenues LEFT JOIN (SELECT GROUP_CONCAT(shop_details.details) details, ledger_id FROM ledger_shops LEFT JOIN (SELECT shops.id shop_id, CONCAT('Shop-', shops.shop_no,' block ',blocks.name, ' ', floors.name) details FROM shops LEFT JOIN blocks ON shops.block_id = blocks.id LEFT JOIN floors ON floors.id = shops.floor_id) AS shop_details
                ON ledger_shops.shop_id = shop_details.shop_id GROUP BY ledger_id) AS details
                ON revenues.ledger_id = details.ledger_id
                LEFT JOIN tenants ON tenants.id = revenues.tenant_id
                WHERE invoice_id = '{$invoice_id}';";
        $payments = DB::select(DB::raw($sql));
        return view('invoice',[
            'invoice_id' => $invoice_id,
            'payments' => $payments
        ]);
    }

    private function generate_invoice_id($shop_id){
        $shop = DB::table('shops')->where('id','=',$shop_id)->first();
        $invoice_id = $shop->project_id . str_pad($shop->shop_no, 3, 0, STR_PAD_LEFT) . date('ymd') . strtoupper(substr(md5(microtime()),rand(0,26),2));
        return $invoice_id;
    }

    public function sale(Request $req){
        $tenants = DB::table('tenants')->get();
        $settings = DB::table('settings')->where('settings_name','=','service_charge_advance')->first();
        if($req->isMethod('GET')){
            return view('staff/sale', [
                'projects' => $this->projects,
                'tenants' => $tenants,
                'advance' => $settings->value
            ]);
        }
        if($req->isMethod('POST')){
            $data = json_decode($_POST['data']);
            $shop_id = $data->shop_id;
            $shop = DB::table('shops')->where('id','=',$shop_id)->first();
            if($shop->tenant_id != null || $shop->type != 'for_sell'){
                $error_msg = "Shop is not for sale.";
            }elseif(!$data->tenant_id){
                $error_msg = "No tenant is selected.";
            }elseif(empty($data->payments->down_payment) && empty($data->payments->no_of_installments)){
                $error_msg = "No down payment or installments.";
            }elseif(!is_numeric($data->payments->down_payment)){
                $error_msg = "Down payment is not a valid amount.";
            }elseif($data->payments->down_payment > $shop->price){
                $error_msg = "Down payment greater than price.";
            }elseif($data->payments->down_payment < $shop->price && empty($data->payments->no_of_installments)){
                $error_msg = "Need to add installment.";
            }
            if(isset($error_msg)){
                return view('staff/sale', [
                    'projects' => $this->projects,
                    'tenants' => $tenants,
                    'advance' => $settings->value,
                    'error_msg' => $error_msg
                ]);
            }
            /** create sale */
            $interval = empty($data->payments->installment_interval) ? 1 : $data->payments->installment_interval;
            $sale_id = DB::table('sales')->insertGetId(
                array(
                    'shop_id' => $data->shop_id,
                    'tenant_id' => $data->tenant_id,
                    'amount' => $shop->price,
                    'down_payment' => $data->payments->down_payment,
                    'no_of_installments' => $data->payments->no_of_installments,
                    'installment_interval' => $interval,
                    'sale_date' => date('Y-m-d')
                )
            );

            $project = DB::table('projects')->where('id','=',$shop->project_id)->first();
            $tenant = DB::table('tenants')->where('id','=',$data->tenant_id)->first();
            $invoice_id = $this->generate_invoice_id($data->shop_id);
            /*adding down payment to revenues table*/
            DB::table('revenues')->insert(array(
                'project_id' => $project->id,
                'shop_id' => $data->shop_id,
                'shop_no' => $shop->shop_no,
                'sale_id' => $sale_id,
                'project' => $project->name,
                'floor' => DB::table('floors')->where('id', '=', $shop->floor_id)->first()->name,
                'block' => DB::table('blocks')->where('id', '=', $shop->block_id)->first()->name,
                'size' => $shop->size,
                'revenue_type' => 'down payment',
                'amount' => $data->payments->down_payment,
                'due_date' => date('Y-m-d'),
                'collection_date' => date('Y-m-d'),
                'tenant_id' => $tenant->id,
                'tenant_name' => $tenant->name,
                'tenant_phone' => $tenant->phone,
                'collected_by' => Auth::id(),
                'invoice_id' => $invoice_id
            ));
            /*adding advance service charges to revenues table*/
            for($i = 0; $i < $settings->value; $i++){
                DB::table('revenues')->insert(array(
                    'project_id' => $project->id,
                    'shop_id' => $data->shop_id,
                    'shop_no' => $shop->shop_no,
                    //'sale_id' => $sale_id,
                    'project' => $project->name,
                    'floor' => DB::table('floors')->where('id', '=', $shop->floor_id)->first()->name,
                    'block' => DB::table('blocks')->where('id', '=', $shop->block_id)->first()->name,
                    'size' => $shop->size,
                    'revenue_type' => 'service charge',
                    'amount' => $shop->service_charge,
                    'due_date' => date('Y-m-01', strtotime("+" . $i . " month")),
                    'collection_date' => date('Y-m-d'),
                    'tenant_id' => $tenant->id,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone,
                    'collected_by' => Auth::id(),
                    'invoice_id' => $invoice_id
                ));
            }

            /** adding installments to installments table */
            if($shop->price > $data->payments->down_payment){
                $amount_per_installment = ($shop->price - $data->payments->down_payment) / $data->payments->no_of_installments;
                for($i = 1; $i <= $data->payments->no_of_installments; $i++){
                    DB::table("sales_installments")->insert(array(
                        'sale_id' => $sale_id,
                        'shop_id' => $shop->id,
                        'tenant_id' => $tenant->id,
                        'amount' => $amount_per_installment,
                        'due_date' => date('Y-m-01', strtotime("+" . ($i * $interval) . " month")),
                    ));
                }
            }

            /** update shop */
            $update_data = [
                'tenant_id' => $tenant->id,
                'type' => 'sold',
            ];
            DB::table('shops')->where('id', '=', $shop->id)->update($update_data);

            return view('staff/sale',[
                'invoice_id' => $invoice_id
            ]);

        }
    }

    public function collectSalesInstallment(Request $req){
        if($req->isMethod('GET')){
            return view('staff/collect-installment', [
                'projects' => $this->projects,
            ]);
        }
        if($req->isMethod('POST')){
            $rev = DB::table('revenues')->where('id','=',$req->input('data'))->whereNull('collected_by')->first();
            $invoice_id = $this->generate_invoice_id($rev->shop_id);
            $user_id = Auth::id();
            $update_data = [
                'collected_by' => $user_id,
                'collection_date' => date('Y-m-d'),
                'invoice_id' => $invoice_id
            ];
            DB::table('revenues')->where('id', '=', $rev->id)->update($update_data);
            return view('staff/collect-installment', [
                'projects' => $this->projects,
                'invoice_id' => $invoice_id
            ]);
        }
    }

    public function ownershipTransfer(Request $req){
        $settings = DB::table('settings')->where('settings_name','=','transfer_fee')->first();
        if($req->isMethod('GET')){
            return view('staff/ownership-transfer', [
                'projects' => $this->projects,
                'transfer_fee' => $settings->value
            ]);
        }
        if($req->isMethod('POST')){
            $shop_id = $req->input('shop');
            $tenant_id = $req->input('transfer_to');
            $amount = $req->input('amount');
            $shop = DB::table('shops')->where('id','=',$shop_id)->first();
            if($shop->tenant_id == $tenant_id || $shop->type != 'sold'){
                die("invalid data");
            }
            /*creating the transfer entry*/
            $transfer_id = DB::table('ownership_transfers')->insertGetId([
                'shop_id' => $shop_id,
                'from_owner' => $shop->tenant_id,
                'to_owner' => $tenant_id,
                'amount' => $amount,
                'transfer_date' => date('Y-m-d H:i:s')
            ]);
            /*adding revenues entry*/
            $invoice_id = $this->generate_invoice_id($shop_id);
            $project = DB::table('projects')->where('id','=',$shop->project_id)->first();
            $tenant = DB::table('tenants')->where('id','=',$shop->tenant_id)->first();
            DB::table('revenues')->insert(array(
                'project_id' => $project->id,
                'shop_id' => $shop_id,
                'shop_no' => $shop->shop_no,
                'transfer_id' => $transfer_id,
                'project' => $project->name,
                'floor' => DB::table('floors')->where('id', '=', $shop->floor_id)->first()->name,
                'block' => DB::table('blocks')->where('id', '=', $shop->block_id)->first()->name,
                'size' => $shop->size,
                'revenue_type' => 'transfer fee',
                'amount' => $amount * $settings->value / 100,
                'due_date' => date('Y-m-d'),
                'collection_date' => date('Y-m-d'),
                'tenant_id' => $shop->tenant_id,
                'tenant_name' => $tenant->name,
                'tenant_phone' => $tenant->phone,
                'collected_by' => Auth::id(),
                'invoice_id' => $invoice_id
            ));
            /** updating shop owner */
            DB::table('shops')->where('id','=',$shop_id)->update(['tenant_id' => $tenant_id]);
            DB::table('shop_attributes')->insert([
                'shop_id' => $shop_id,
                'attribute_name' => 'tenant',
                'attribute_value' => $tenant_id,
                'updated_by' => Auth::id(),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return view('staff/ownership-transfer', [
                'invoice_id' => $invoice_id
            ]);
        }
    }

    public function collectRevenue( Request $req){
        if($req->post('id')){
            $revenue = DB::table('revenues')->where('id', '=', $req->post('id'))->whereNull('invoice_id')->first();
            if($revenue){
                $invoice_id = $this->generate_invoice_id($revenue->shop_id);
                $user_id = Auth::id();
                $update_data = [
                    'collected_by' => $user_id,
                    'collection_date' => date('Y-m-d'),
                    'invoice_id' => $invoice_id
                ];
                DB::table('revenues')->where('id', '=', $req->post('id'))->update($update_data);
                return redirect(url('/'))->with('invoice_id', $invoice_id);
            }
        }

        return redirect(url('/'));
    }
}
