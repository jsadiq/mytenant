<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Mamun",
            'email' => 'almamun@gmail.com',
            'password' => bcrypt('manager123'),
            'username' => "manager",
        ]);
    }
}
