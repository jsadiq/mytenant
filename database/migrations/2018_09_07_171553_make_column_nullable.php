<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeColumnNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function(Blueprint $table){
			$table->dropColumn('type');
		});
		Schema::table('shops', function(Blueprint $table){
			$table->integer('tenant_id')->nullable()->change();
            $table->float('rent', 8, 2)->nullable()->change();
            $table->float('price', 8, 2)->nullable()->change();
			$table->float('service_charge', 8, 2)->nullable()->change();
            $table->string('type');
			$table->boolean('disable_bulk_update')->nullable()->change();
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
