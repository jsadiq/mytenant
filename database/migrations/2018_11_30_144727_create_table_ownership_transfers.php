<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOwnershipTransfers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ownership_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id');
            $table->integer('from_owner');
            $table->integer('to_owner');
            $table->double('amount');
            $table->date('transfer_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ownership_transfers');
    }
}
