<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRevenues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id');
            $table->string('shop_no');
            $table->integer('sale_id');
            $table->string('project');
            $table->string('floor');
            $table->string('size');
            $table->string('revenue_type');
            $table->float('amount');
            $table->date('due_date');
            $table->date('collection_date');
            $table->integer('tenant_id');
            $table->string('tenant_name');
            $table->string('tenant_phone');
            $table->integer('collected_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenues');
    }
}
