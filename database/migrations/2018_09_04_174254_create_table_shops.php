<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('floor_id');
            $table->integer('block_id');
            $table->string('shop_no');
            $table->integer('tenant_id');
            $table->float('rent', 8, 2);
            $table->float('price', 8, 2);
            $table->float('service_charge', 8, 2);
            $table->enum('type', ['sale', 'rent']);
            $table->integer('size');
            $table->boolean('disable_bulk_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
